#!/bin/bash

get_pid=$(ps auxf|grep pzubrytskyi|grep ssh-agent|awk '{print $2}')
agent_socket="/home/pzubrytskyi/.ssh/agent.sock"
source_file="/home/pzubrytskyi/.ssh/agent_init.sh"

[[ "$get_pid" ]] || { rm -f "$agent_socket"; ssh-agent -a "$agent_socket" > "$source_file" ; }
[[ -f "$source_file" ]] && source "$source_file"
[[ $(ssh-add -l |grep id_rsa) ]] || ssh-add
