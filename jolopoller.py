#!/usr/bin/env python

import json
import numpy
import urllib2
import sys
import requests
import re
import os
import ast


def get_configs():
    global config_file
    config_file = os.path.join(os.path.dirname(__file__),"/var/tmp/jolometrics.txt")
    global sorted_dataset
    sorted_dataset = []
    global request_timeout
    request_timeout = 3

def get_input():
    try:
        global port
        port=sys.argv[1]
    except Exception:
        pass

def sort_config_to_list():
    with open(config_file,'r') as fp:
        for line in fp.readlines():
            if re.search(r'#',line) is None:
                tmp_line = re.sub(r'\]\'','',re.sub(r'\'\[','', re.sub(r'JVMINFO =','',line).strip())).strip(',')
                if not not tmp_line:
                    sorted_dataset.append(ast.literal_eval(tmp_line))

def parse_data_from_jolo():
    try:
        url = 'http://127.0.0.1:{}/jolokia/'.format(port)
        headers = {'content-type': 'application/json'}
        r = requests.post(url, data=json.dumps(sorted_dataset), headers=headers, timeout=request_timeout)
        json_data = json.loads(r.text)

        # print json.dumps(json_data, indent=4)
        for dicts in json_data:
            if 'path' in dicts['request']:
                print str(dicts['request']['mbean']).lstrip('java.lang:name=') + str(dicts['request']['attribute']) + str(dicts['request']['path']) + "\n" + str(dicts['value'])
            elif 'value' in dicts:
                print str(dicts['request']['mbean']).lstrip('java.lang:name=') + str(dicts['request']['attribute']) + "\n" + str(dicts['value'])
    except Exception:
        pass

def get_discovery_items():
    try:
        tomcat_dir = os.listdir("/data/tomcat")
        apps = []
        data = {}
        for dir in tomcat_dir:
            apps.append({'{#APP_NAME}':dir})
        data.update({'data':apps})
        print json.dumps(data, indent=4)
    except Exception:
        pass

if __name__ == "__main__":
    if len(sys.argv) == 2:
        if 'discovery' == sys.argv[1]:
            get_discovery_items()
        else:
            get_configs()
            get_input()
            sort_config_to_list()
            parse_data_from_jolo()
    else:
        print "Provide arguments!"
        sys.exit(2)
    sys.exit(0)
