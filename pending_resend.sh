#!/bin/sh

task_rake="/onapp/interface/lib/tasks/pending.rake"
crontab_table="/etc/crontab"
crontab_mins=${1:-"10"}
onapp_cp_version="$(rpm -q onapp-cp)"

cat > $task_rake << EOF


require 'on_app/mutexed_task'

namespace :pending do
  desc "Resend existing scheduled tasks to rabbitmq queue"
  task resend: :environment do

    blocker_processes =
      ['rake pending:resend'].select do |process_name|
        OnApp::Utility::Process.another_running?(process_name)
    end 

    if blocker_processes.present?
      exit
    else
      %x( sudo rabbitmqctl purge_queue onapp.tasks.backups_supervisor ) 
      TransactionService.resend_transactions(Transaction.by_status(:pending))
    end     
   
  end

end
EOF

sed -i "/pending:resend/d" $crontab_table
echo "*/$crontab_mins * * * * onapp cd /onapp/interface; [ "'"$(rpm -q onapp-cp)"'" == "'"'$onapp_cp_version'"'" ] && RAILS_ENV=production rake pending:resend" >> $crontab_table

chown onapp. $task_rake

#for o in 1 2 3; do case $o in 1) p="5";; 2) p="10";; 3) p="15";; esac ; x=$(( o + p )); ssh root@cp"$o" "wget -qO- https://gitlab.com/hyperonfarm/shared/raw/master/pending_resend.sh|bash -s $x"; done
