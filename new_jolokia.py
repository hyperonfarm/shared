#!/usr/bin/env python

import json
import requests
import re
import os
import ast
import argparse
import yaml
import pwd
import grp

#READ THE CONFIGURATION FILE, MUST PRODUCE A DICTIONARY
def read_conf():
    pwd = os.path.dirname(os.path.realpath(__file__))
    cfg_subdir = 'jo_poller_cfg'
    for filename in os.listdir(pwd + '/' + cfg_subdir):
        if re.search('jo_poller', filename):
            if re.search('env.yml', filename):
                yaml_path = os.path.join(pwd,cfg_subdir,filename)
                break
            else:
                yaml_path = os.path.join(pwd,cfg_subdir,'jo_poller.yml')
    with open(yaml_path,'r') as ymlcfg:
        yaml_cfg = yaml.load(ymlcfg)
    return yaml_cfg
##################

#FUNCTION PARSES CONFIG FILE AND RETURNS A SORTED LIST OF DICTIONARIES THAT CAN BE PASSED TO REQUEST
def parse_config(jo_dataset):
    dataset = []
    for line in jo_dataset.splitlines():
        if re.search(r'#',line) is None:
            tmp_line = re.sub(r'\]\'','',re.sub(r'\'\[','', re.sub(r'JVMINFO =','',line).strip())).strip(',')
            if not not tmp_line:
                dataset.append(ast.literal_eval(tmp_line))
    return dataset
##################

#FUNCTION CREATES FOLDERS FOR EACH APP AND STORES
def metrics_storage(path,app_name):
    if not os.path.exists(path):
        uid = pwd.getpwnam(yaml['metrics_consumer']).pw_uid
        gid = grp.getgrnam(yaml['metrics_consumer']).gr_gid
        os.makedirs(path)
        os.chown(path, uid, gid)
        os.chmod(path, 0o755)
    if app_name in list_apps(yaml['existing_apps']):
        if not os.path.exists(path + '/' + app_name):
                uid = pwd.getpwnam(yaml['metrics_consumer']).pw_uid
                gid = grp.getgrnam(yaml['metrics_consumer']).gr_gid
                os.makedirs(path + '/' + app_name)
                os.chown(path + '/' + app_name,uid,gid)
                os.chmod(path + '/' + app_name, 0o755)
##################

#FUNCTION GETS PATH TO FILE AND WRITES VALUE INTO IT
def file_io(path,filename,value,mode):
    if mode == 'r':
        file = open(os.path.join(path,filename),mode)
        print file.readlines()[0].strip()
        file.close()
    elif mode == 'w':
        uid = pwd.getpwnam(yaml['metrics_consumer']).pw_uid
        gid = grp.getgrnam(yaml['metrics_consumer']).gr_gid
        file = open(os.path.join(path,filename),mode)
        file.write(value)
        file.close()
        os.chown(path + '/' + filename,uid,gid)
        os.chmod(path + '/' + filename, 0o644)
##################

#FUNCTION TAKES JSON AND RETURNS A ONE DIMENSIONAL DICTIONARY WITH MBEAN-VALUE PAIRS
def json_strip(json_struct):
    mbean_name_value = {}
    sorted_dataset = parse_config(yaml['jolokia_data'])
    mbean_roots = []
    for dict in sorted_dataset:
        mbean_roots.append(dict['mbean'].split(':')[0])
    sort_uniq_mbean_roots = sorted(set(mbean_roots))
    for dict in json_struct:
        if dict['status'] == 200:
            _regexp = ['type=','name=',',',':'] + sort_uniq_mbean_roots
            mbean = re.sub(r'|'.join(map(re.escape,_regexp)),'',str(dict['request']['mbean']))
            attribute = str(dict['request']['attribute'])
            if 'path' in dict['request'] and 'value' in dict:
                path = str(dict['request']['path'])
                full_mbean = re.sub(r'\s+','_',mbean + attribute + path)
                value = str(dict['value'])
                mbean_name_value.update({full_mbean:value})
            elif 'value' in dict:
                full_mbean = re.sub(r'\s+','_',mbean + attribute)
                value = str(dict['value'])
                mbean_name_value.update({full_mbean:value})
    return mbean_name_value
##################

#FUNCTION POLLS THE JOLOKIA FRONTEND AND GETS JSON OBJECT STRUCTURE WITH ALL METRICS IN CONFIG
def jolo_to_json(port,in_timeout):
    sorted_dataset = parse_config(yaml['jolokia_data'])
    url = 'http://127.0.0.1:{0}/jolokia/'.format(port)
    headers = {'content-type': 'application/json'}
    r = requests.post(url, data=json.dumps(sorted_dataset), headers=headers, timeout=in_timeout)
    return json.loads(r.text)
##################

#FUNCTION THAT DISCOVERS THE APPS
def get_discovery_items(apps_folder):
    try:
        tomcat_dir = list_apps(apps_folder)
        apps = []
        data = {}
        for dir in tomcat_dir:
            apps.append({'{#APP_NAME}':dir})
        data.update({'data':apps})
        print json.dumps(data, indent=4)
    except Exception:
        pass
##################

#MIDDLEWARE FUNCTION THAT STORES METRICS ON DISK
def store_metrics(app_name):
    try:
        port = find_port(app_name)
        metrics_storage(yaml['metrics_root_path'],app_name)
        received_metrics=json_strip(jolo_to_json(port,yaml['request_timeout']))
        for mbean in received_metrics:
            file_io(yaml['metrics_root_path'] + '/' + app_name,mbean,received_metrics[mbean]+"\n","w")
        print "OK"
    except Exception:
        print "ERROR!!!"
##################

#FUNCTION GETS AND PRINTS STORED METRIC
def get_metrics(path,app_name,metric_name):
    filename = app_name + "/" + metric_name
    if os.path.isfile(path + '/' + filename):
        file_io(path,filename,"","r")
##################
