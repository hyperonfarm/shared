#!/bin/bash

logger "$0 - Started"

CONF_DEFAULT='onapp-cp-install.conf'
LOG_DEFAULT='onapp-cp-install.log'

PRIVATE_CLOUD=0

SSL_CERT_DIR_DEFAULT='/etc/pki/tls'
SSL_CERT_COUNTRY_NAME_DEFAULT=UK
SSL_CERT_COMMON_NAME_DEFAULT='cp.onapp.com'
SSL_CERT_ORGANIZATION_NAME_DEFAULT='OnApp Limited'
SSL_CERT_ORGANIZATION_ALUNITNAME_DEFAULT='OnApp Cloud'

PWD=`pwd`
RUN_PATH=`dirname $0`

CP_VERSION_DEFAULT="2.0.1"

MYSQL_ADAPTER_DEFAULT='mysql2'
MYSQL_WAIT_TIMEOUT_DEFAULT=604800
MYSQL_MAX_CONNECTIONS_DEFAULT=500
MYSQL_PORT_DEFAULT=3306
NET_CORE_SOMAXCONN_DEFAULT=2048
RUBY_GC_MALLOC_LIMIT_DEFAULT=140000000
RAILS_ENV=production
MAX_UPLOAD_SIZE_DEFAULT=1073741824
MAX_UPLOAD_SIZE_MAX=2147483647
HTTP_PORT=80
HTTPS_PORT=443
HA_ENABLED=0
LOAD_BALANCER_ENABLED=0
HA_HTTP_PORT=10080
HA_HTTPS_PORT=10443
HA_REDIS_PORT=6479
HA_MYSQL_PORT=3406
WITH_MARIADB=0
WITH_REDIS=1
REDIS="redis"

# OnApp MySQL connection default data (database.yml)
ONAPP_CONN_WAIT_TIMEOUT_DEFAULT='15'
ONAPP_CONN_POOL_DEFAULT='30'
ONAPP_CONN_RECONNECT_DEFAULT='true'
ONAPP_CONN_ENCODING_DEFAULT='utf8'
ONAPP_CONN_SOCKET_DEFAULT='/var/lib/mysql/mysql.sock'

PROGLANG_DEFAULT='ruby'

WEBSERVER_DEFAULT='httpd'
HTTPD_GRANTED=''

PACKAGES_EXCLUDE_DEFAULT='ruby* libyaml* corosync* corosynclib* pacemaker*'
PACKAGES_EXCLUDE_EL6_DEFAULT='ruby* libyaml* corosync* corosynclib* pacemaker*'
PACKAGES_EXCLUDE_EL7_DEFAULT='ruby* libyaml*'
RM_PACKAGES_EXCLUDE=

ONAPP_MYSQL_DATABASE_CONN_CONF='/onapp/interface/config/database.yml'
ONAPP_CONF='/onapp/interface/config/on_app.yml'
ONAPP_ADMIN_DATA_CONF='/onapp/interface/db/fixtures/admin.yml'
ONAPP_REDIS_CONF="/onapp/interface/config/redis.yml"
ONAPP_CONFIGURATION="/onapp/configuration"
ONAPP_MYSQL="/onapp/onapp-mysql"
ONAPP_USER=onapp
ONAPP_CUSTOM_VERSION=
UPDATE_FROM=

MYSQL_PKG='mysql'
MYSQLD='mysqld'
MYSQL_NAME="MySQL Server"
MYSQL_HOST_DEFAULT='localhost'
MYSQL_DB='onapp'
MYSQL_DB_NEW=
MYSQL_USER='root'
MY_CNF='/etc/my.cnf'
SKIP_MYSQL_DUMP=0

BASH_PROFILE="/root/.bash_profile"

ADMIN_PASSWD=
ADMIN_FIRSTNAME=
ADMIN_LASTNAME=
ADMIN_EMAIL=
ADMIN_DATA=

RBT_OPTS=
RBT_HOST=

SNMP_TRAP_IPS_NEW=

CONF=
AUTO=0
YUMUPDATE=0
NOSERVICES=0

YUM_OPTS_ADD=

CROND="crond"
MONIT="monit"

MYSQL=1
MARIADB=0
COMMUNITY=0
PERCONA=0
PERCONA_CLUSTER=0
ONAPP_MYSQL_OPTS=

ADD_BASE_TEMPLATES=0
WGET_TRIES=5

SKIP_REPOS=
RAKE_TASKS=

HA_INSTALL=0

MYSQL_INSTALLED=0
MYSQL_RESTART=0

ONAPP_CP_INSTALLED=0



TRY_DOWNLOAD=1
if [ $AUTO -eq 0 -a $PRIVATE_CLOUD -eq 0 -a -z "${QUICK_UPDATE+x}" ]; then
    log "Installer can try to initiate images, templates, ISOs which are required by OnApp."
    log "You should manually put:
 - Recovery images (from $RECOVERY_TEMPLATES_DIR), FreeBSD boot ISO (from $FREEBSD_ISO_DIR) and GRUB ISOs (from $FREEBSD_ISO_DIR) - into Backup/Template server's /data directory;
 - LoadBalancer, CDN and ApplicationServer templates from $LB_TEMPLATE_DIR into 'Templates' directory, if separate Backup/Template server is configured."
    log "Please visit Control Panel Settings->Configuration->Backup/Templates for the details."
    fflush_stdin
    read_no "Do you wish to proceed with download"
    if [ $? -eq 0 ]; then
        log "Download cancelled by user" "WARNING"
        TRY_DOWNLOAD=0
    fi
fi

[ -n "${QUICK_UPDATE+x}" ] && TRY_DOWNLOAD=0

if [ $TRY_DOWNLOAD -eq 1 -a $PRIVATE_CLOUD -eq 0 ]; then

    # Initiating Recovery templates download
    if [ "x${RECOVERY_TEMPLATES}" != "x" -a "x${RECOVERY_TEMPLATES_DIR}" != "x" ]; then
        if [ ! -d $RECOVERY_TEMPLATES_DIR ]; then
            mkdir -p $RECOVERY_TEMPLATES_DIR
        fi
        chown ${ONAPP_USER}:${ONAPP_USER} $RECOVERY_TEMPLATES_DIR

        if ! ps ax|grep wget|grep -v grep|grep recovery >/dev/null 2>&1; then
            log "Generating Recovery images download list"
            rm -f $RECOVERY_TEMPLATES_DIR/recovery-templates-url.list
            for TEMPLATE in $RECOVERY_TEMPLATES; do
                echo "${TEMPLATES_URL}/${TEMPLATE}" >> $RECOVERY_TEMPLATES_DIR/recovery-templates-url.list
            done
            if [ -f $RECOVERY_TEMPLATES_DIR/recovery-templates-url.list ]; then
                log "Initiate Recovery images download"
                wget -b -N -i $RECOVERY_TEMPLATES_DIR/recovery-templates-url.list -o $RECOVERY_TEMPLATES_DIR/recovery-templates-download.log -P $RECOVERY_TEMPLATES_DIR >/dev/null 2>&1
            fi
        else
            log "Recovery images seems to be being downloaded at the moment." "WARNING"
        fi
    fi

    # Initiating GRUB ISOs download
    if [ "x${GRUB_ISOS}" != "x" -a "x${GRUB_ISO_DIR}" != "x" ]; then
        if [ ! -d $GRUB_ISO_DIR ]; then
            mkdir -p $GRUB_ISO_DIR
        fi
        chown ${ONAPP_USER}:${ONAPP_USER} $GRUB_ISO_DIR

        if ! ps ax|grep wget|grep -v grep|grep -i grub >/dev/null 2>&1; then
            log "Generating GRUB ISOs download list"
            rm -f $GRUB_ISO_DIR/grub-isos-url.list
            for TEMPLATE in $GRUB_ISOS; do
                echo "${TEMPLATES_URL}/${TEMPLATE}" >> $GRUB_ISO_DIR/grub-isos-url.list
            done
            if [ -f $GRUB_ISO_DIR/grub-isos-url.list ]; then
                log "Initiate GRUB ISOs download"
                wget -b -N -i $GRUB_ISO_DIR/grub-isos-url.list -o $GRUB_ISO_DIR/grub-isos-download.log -P $GRUB_ISO_DIR >/dev/null 2>&1
            fi
        else
            log "GRUB ISOs seems to be being downloaded at the moment." "WARNING"
        fi
    fi

    # initiating freebsd iso download
    if [ "x${FREEBSD_ISO}" != "x" ]; then
        if [ ! -d $FREEBSD_ISO_DIR ]; then
            mkdir -p $FREEBSD_ISO_DIR
        fi
        chown ${ONAPP_USER}:${ONAPP_USER} $FREEBSD_ISO_DIR

        if ! ps ax|grep wget|grep -v grep|grep 'boot-freebsd' >/dev/null 2>&1; then
            log "Generating FreeBSD boot ISO download list"
            rm -f $FREEBSD_ISO_DIR/freebsd-iso-url.list
            for ISO in $FREEBSD_ISO; do
                echo "${ISO}" >> $FREEBSD_ISO_DIR/freebsd-iso-url.list
            done
            if [ -f $FREEBSD_ISO_DIR/freebsd-iso-url.list ]; then
                log "Initiate FreeBSD boot ISO download"
                wget -b -N -i $FREEBSD_ISO_DIR/freebsd-iso-url.list -o $FREEBSD_ISO_DIR/freebsd-iso-download.log -P $FREEBSD_ISO_DIR >/dev/null 2>&1
            fi
        else
            log "FreeBSD boot ISO seems to be being downloaded at the moment." "WARNING"
        fi
    fi

    # initiating LBVA download and add it into the database
    if [ "x${LB_TEMPLATE_DIR}" != "x" -a "x${LB_TEMPLATE}" != "x" ]; then
        if [ ! -d $LB_TEMPLATE_DIR ]; then
            mkdir -p $LB_TEMPLATE_DIR
        fi
        chown ${ONAPP_USER}:${ONAPP_USER} $LB_TEMPLATE_DIR

        if ! ps ax|grep wget|grep -v grep|grep 'lbva' >/dev/null 2>&1; then
            log "Generating LoadBalancer template download list"
            rm -f $LB_TEMPLATE_DIR/lbva-template-url.list

            echo "${TEMPLATES_URL}/Linux/${LB_TEMPLATE}" >> $LB_TEMPLATE_DIR/lbva-template-url.list
            if [ -f $LB_TEMPLATE_DIR/lbva-template-url.list ]; then
                log "Initiate LoadBalancer template download"
                wget -b -N -i $LB_TEMPLATE_DIR/lbva-template-url.list -o $LB_TEMPLATE_DIR/lbva-template-download.log -P $LB_TEMPLATE_DIR >/dev/null 2>&1
            fi
        else
            log "LoadBalancer template seems to be being downloaded at the moment." "WARNING"
        fi
    fi

    # initiating CDN download
    if [ "x${CDN_TEMPLATE_DIR}" != "x" -a "x${CDN_TEMPLATE}" != "x" ]; then
        if [ ! -d $CDN_TEMPLATE_DIR ]; then
            mkdir -p $CDN_TEMPLATE_DIR
        fi
        chown ${ONAPP_USER}:${ONAPP_USER} $CDN_TEMPLATE_DIR

        if ! ps ax|grep wget|grep -v grep|grep 'debian-6.0-x64' >/dev/null 2>&1; then
            log "Generating CDN templates download list"
            rm -f $CDN_TEMPLATE_DIR/cdn-template-url.list

            echo "${TEMPLATES_URL}/Linux/${CDN_TEMPLATE}" >> $CDN_TEMPLATE_DIR/cdn-template-url.list
            if [ -f $CDN_TEMPLATE_DIR/cdn-template-url.list ]; then
                log "Initiate CDN template download"
                wget -b -N -i $CDN_TEMPLATE_DIR/cdn-template-url.list -o $CDN_TEMPLATE_DIR/cdn-template-download.log -P $CDN_TEMPLATE_DIR >/dev/null 2>&1
            fi
        else
            log "CDN template seems to be being downloaded at the moment." "WARNING"
        fi
    else
        log "Undefined CDN_TEMPLATE_DIR='${CDN_TEMPLATE_DIR}' or CDN_TEMPLATE='${CDN_TEMPLATE}'"
    fi

    # initiating Windows SmartServer drivers download
    if [ "x${WINDOWS_SMART_DRIVERS}" != "x" ]; then
        if [ ! -e $WINDOWS_SMART_DRIVERS_DIR ]; then
            mkdir -d $WINDOWS_SMART_DRIVERS_DIR
        fi
        chown ${ONAPP_USER}:${ONAPP_USER} $WINDOWS_SMART_DRIVERS_DIR

        if ! ps ax|grep wget|grep -v grep|grep 'smart_server_windows' >/dev/null 2>&1; then
            log "Generating Windows SmartServer drivers download list"
            rm -f $WINDOWS_SMART_DRIVERS_DIR/windows-smart-drivers-url.list
            for ISO in $WINDOWS_SMART_DRIVERS; do
                echo "${ISO}" >> $WINDOWS_SMART_DRIVERS_DIR/windows-smart-drivers-url.list
            done
            if [ -f $WINDOWS_SMART_DRIVERS_DIR/windows-smart-drivers-url.list ]; then
                log "Initiate Windows SmartServer drivers download"
                wget --reject 'index.html*' --no-directories --no-parent --recursive --no-host-directories -b -N -i $WINDOWS_SMART_DRIVERS_DIR/windows-smart-drivers-url.list -o $WINDOWS_SMART_DRIVERS_DIR/windows-smart-drivers-download.log -P $WINDOWS_SMART_DRIVERS_DIR >/dev/null 2>&1
            fi
        else
            log "Windows SmartServer drivers seems to be being downloaded at the moment." "WARNING"
        fi
    fi

    # Initiating CloudBoot Baremetal recovery images download
    if [ "x${CLOUDBOOT_BM_RECOVERY_DIR}" != "x" -a "x${CLOUDBOOT_BM_RECOVERY}" != "x" ]; then
        if [ ! -d $CLOUDBOOT_BM_RECOVERY_DIR ]; then
            mkdir -p $CLOUDBOOT_BM_RECOVERY_DIR
        fi
        chown ${ONAPP_USER}:${ONAPP_USER} $CLOUDBOOT_BM_RECOVERY_DIR

        if ! ps ax|grep wget|grep -v grep|grep 'recovery-baremetal' >/dev/null 2>&1; then
            log "Generating CloudBoot BM recovery images download list"
            rm -f $CLOUDBOOT_BM_RECOVERY_DIR/cb-bm-recovery-imgs-url.list

            for IMG in $CLOUDBOOT_BM_RECOVERY; do
                echo "${TEMPLATES_URL}/${IMG}" >> $CLOUDBOOT_BM_RECOVERY_DIR/cb-bm-recovery-imgs-url.list
            done
            if [ -f $CLOUDBOOT_BM_RECOVERY_DIR/cb-bm-recovery-imgs-url.list ]; then
                log "Initiate CloudBoot BM recovery images download"
                wget -b -N -i $CLOUDBOOT_BM_RECOVERY_DIR/cb-bm-recovery-imgs-url.list -o $CLOUDBOOT_BM_RECOVERY_DIR/cb-bm-recovery-imgs-download.log -P $CLOUDBOOT_BM_RECOVERY_DIR >/dev/null 2>&1
            fi
        else
            log "CloudBoot BM recovery images seem to be being downloaded at the moment." "WARNING"
        fi
    fi

    # initiating ASVA download
    if [ "x${ASVA_TEMPLATE_DIR}" != "x" -a "x${ASVA_TEMPLATE}" != "x" ]; then
        if [ ! -d $ASVA_TEMPLATE_DIR ]; then
            mkdir -p $ASVA_TEMPLATE_DIR
        fi
        chown ${ONAPP_USER}:${ONAPP_USER} $ASVA_TEMPLATE_DIR

        if ! ps ax|grep wget|grep -v grep|grep 'asva' >/dev/null 2>&1; then
            log "Generating Application Server template download list"
            rm -f $ASVA_TEMPLATE_DIR/asva-template-url.list

            echo "${TEMPLATES_URL}/Linux/${ASVA_TEMPLATE}" >> $ASVA_TEMPLATE_DIR/asva-template-url.list
            if [ -f $ASVA_TEMPLATE_DIR/asva-template-url.list ]; then
                log "Initiate Application Server template download"
                wget -b -N -i $ASVA_TEMPLATE_DIR/asva-template-url.list -o $ASVA_TEMPLATE_DIR/asva-template-download.log -P $ASVA_TEMPLATE_DIR >/dev/null 2>&1
            fi
        else
            log "Application Server template seems to be being downloaded at the moment." "WARNING"
        fi
    else
        log "Undefined ASVA_TEMPLATE_DIR='${ASVA_TEMPLATE_DIR}' or ASVA_TEMPLATE='${ASVA_TEMPLATE}'"
    fi
fi

if [ "x${ONAPP_VERSION}" = "x" ]; then
    ONAPP_VERSION=$CP_VERSION
fi

[ $NOSERVICES -eq 1 ] && log "Please note the monit, onapp and $HTTPD services aren't running. Please think about starting them when you are done with the $RUN_MODE." "WARNING"

logger "$0 - Finished."
log "`date '+%F %H:%M'` Finished Control Panel version '$ONAPP_VERSION' $RUN_MODE" "SUCCESS"

exit 0