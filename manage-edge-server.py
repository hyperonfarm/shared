#!/usr/bin/env python
"""
A tool for OnApp operators to self-provision their own bare metal servers.
"""

import os.path
import sys
from pprint import pprint
from subprocess import Popen, PIPE
from uuid import uuid1
from xmlrpclib import ServerProxy, _Method, Fault

CONFIG_FILE = os.path.expanduser("~/.onapprc")
API_URL     = "https://api.onappcdn.com/core/xmlrpc"

class OnAppRPC(ServerProxy):

    def __init__(self, url, user, passwd):
        ServerProxy.__init__(self, url, allow_none=True)
        self.user   = user
        self.passwd = passwd

    def __getattr__(self, name):
        return OnAppRPCMethod(self._ServerProxy__request,
                              name,
                              self.user,
                              self.passwd)

class OnAppRPCMethod(_Method):

    def __init__(self, send, name, user, passwd):
        _Method.__init__(self, send, name)
        self.user   = user
        self.passwd = passwd

    def __getattr__(self, name):
        return OnAppRPCMethod(self._Method__send,
                              "%s.%s" % (self._Method__name, name),
                              self.user,
                              self.passwd)

    def __call__(self, *args, **kwargs):
        if "su" in kwargs:
            username = "%s?su=%s" % (self.user, kwargs["su"])
        else:
            username = self.user
        return self._Method__send(self._Method__name,
                                  (username, self.passwd) + args)

def main(args):

    global rpc

    if not args:
        print_usage_doc()
        sys.exit(0)

    cmdline = list(args)
    command = cmdline.pop(0)

    if command in globals():
        if command != "login":
            # instantiate the rpc object for all commands except login()
            rpc = get_rpc()

        command_func = globals()[command]
        command_func(*cmdline)
    else:
        print_usage_doc()
        sys.exit(1)

def print_usage_doc():
    print usage_doc

def enumerate():
    clouds = rpc.clouds.get({})["results"]

    print "Clouds & Locations"
    fmt = "{country:10s} {city:20s} {location_id:10s} {cloud_id:10s}"
    print fmt.format(country="Country", city="City",
                     location_id="Loc ID", cloud_id="Cloud ID")

    for cloud in clouds:
        locations = cloud.get("locations", [])

        for location in locations:
            print fmt.format(
                country     = location["country"],
                city        = location["city"],
                location_id = str(location["id"]),
                cloud_id    = str(cloud["id"])
            )

    print
    print "Edges"
    fmt = "{edge_id:10s} {ip:15s} {country:10s} {city:20s} {status:10s} {mart_status:11s} {edge_type:10s}"
    print fmt.format(edge_id="ID", ip="IP", country="Country", city="City",
                     status="Status", mart_status="Marketplace", edge_type="Type")

    edges = rpc.edges.get({})["results"]
    for edge in edges:
        print fmt.format(
            edge_id     = str(edge["id"]),
            ip          = edge["ip"],
            city        = edge["location"]["city"],
            country     = edge["location"]["country"],
            status      = edge["status"],
            mart_status = edge["martStatus"],
            edge_type   = "HTTP" if edge["httpPullOn"] else "STREAM"
        )


def login(username, password):

    try:
        rpc = OnAppRPC(API_URL, username, password)
        rpc.auth.authenticate()

        with open(CONFIG_FILE, "w") as fp:
            fp.write(username + " " + password)

        print "Successfully logged in."

    except Fault as e:
        print "Error verifying login:\n%s" % e


def get_rpc():
    if not os.path.exists(CONFIG_FILE):
        print "You must run %s login <api username> <api password> first" % sys.argv[0]
        sys.exit(1)

    username, password = open(CONFIG_FILE).read().split(" ")
    rpc = OnAppRPC(API_URL, username, password)

    return rpc


def register(edge_ip, location_id, disk_space, port_speed, marketplace, streaming):

    location_id = int(location_id)
    disk_space  = int(disk_space)
    port_speed  = int(port_speed)
    mart_status = "ENROLLING" if marketplace == "yes" else "PRIVATE"
    streaming   = True if streaming == "yes" else False

    try:
        cloud    = rpc.clouds.get({"locations": [ location_id ]})["results"][0]
        operator = cloud["operator"]
        location = [ l for l in cloud["locations"] if l["id"] == location_id ][0]
    except IndexError:
        raise("Couldn't obtain cloud for location %d" % cloud)

    edge_name = "%s-%s-%s-%s" % (operator["companyName"],
                                 location["country"],
                                 location["city"],
                                 edge_ip)
    edge_param = {
            "operator"          : { "id": operator["id"] },
            "cloud"             : { "id": cloud["id"] },
            "location"          : { "id": location_id },
            "ip"                : edge_ip,
            "serviceIp"         : edge_ip,
            "httpPullOn"        : True,
            "httpPullCacheLimit": disk_space,
            "speedLimit"        : port_speed,
            "martStatus"        : mart_status,
            "name"              : edge_name
        }

    if streaming:
        edge_param["httpPullOn"] = False
        edge_param["streamOn"] = True
        edge_param["wowzaConfig"] = {
                "vhostHostPortProcessorCount": 6,
                "vhostIdleWorkersWorkerCount": 8,
                "vhostNetConProcessorCount": 10,
                "vhostHandlerThreadPoolSize": 12,
                "vhostTransportThreadPoolSize": 14
            }

    edge_id = rpc.edges.create(edge_param)
    pprint(rpc.edges.get({"id": edge_id})["results"][0])


def mount(edge_ip, device_name, bay_no):

    device_uuid = uuid1()

    if not device_name.startswith("/dev/"):
        raise Exception("Device name should start with /dev/")

    p = Popen("ssh root@{edge_ip} mount".format(**locals()),
              shell=True, stdout=PIPE)
    stdout, stderr = p.communicate()
    if device_name in stdout:
        raise Exception("%s is already mounted, aborting for safety reasons" % device_name)

    p = Popen("ssh root@{edge_ip} mkfs.ext4 -F -U {device_uuid} -m 0 {device_name} -L bay-{bay_no}" \
                .format(**locals()),
              shell=True)
    _ = p.communicate()
    if p.returncode != 0:
        raise Exception("mkfs returned %d" % p.returncode)

    p = Popen("ssh root@{edge_ip}".format(**locals()), shell=True, stdin=PIPE)
    p.communicate(mount_commands.format(**locals()))


def install(edge_ip):
    edge_id = get_edge_id(edge_ip)
    p       = Popen([ "ssh", "root@%s" % edge_ip ], stdin=PIPE)

    p.communicate(install_commands.format(**locals()))


def activate(edge_ip):
    edge_id = get_edge_id(edge_ip)
    rpc.edges.update(edge_id, { "status": "ACTIVE" })


def enrol(edge_ip):
    edge_id = get_edge_id(edge_ip)
    rpc.edges.update(edge_id, { "martStatus": "ENROLLING" })


def withdraw(edge_ip):
    edge_id = get_edge_id(edge_ip)
    rpc.edges.update(edge_id, { "martStatus": "PRIVATE" })


def speed(edge_ip, speed):
    edge_id = get_edge_id(edge_ip)

    print "Current speed", rpc.edges.get({"id":edge_id })["results"][0]["speedLimit"], "Mbps"
    print "New speed", rpc.edges.update(edge_id, { "speedLimit": int(speed) })["speedLimit"], "Mbps"


def delete(edge_ip):

    edge_id     = get_edge_id(edge_ip)
    edge_is_up  = False

    print "Checking if edge server is up."
    healths = rpc.mon.getEdgeMetrics()
    for health in healths:
        if (health["edge"]["id"] == edge_id and
            health["healthStatus"] == "OK"):
            edge_is_up = True

    if edge_is_up:
        print "Edge server is up. Please shut down the server and wait a few minutes before trying again."
        sys.exit(1)

    print "********************************************************"
    print "ARE YOU SURE YOU WANT TO DELETE EDGE %s ?" % edge_ip
    print "TYPE 'YES' TO CONTINUE OR CTRL-C TO ABORT."
    print "********************************************************"
    confirm = raw_input()
    if confirm != "YES":
        print "Aborting."
        sys.exit(1)

    rpc.edges.delete(edge_id)
    print "Edge deleted."


def pause(edge_ip):
    edge_id = get_edge_id(edge_ip)
    rpc.edges.update(edge_id, {"status": "PAUSED"})


def get_edge_id(edge_ip):
    try:
        return rpc.edges.get({"ip": edge_ip})["results"][0]["id"]
    except IndexError:
        raise Exception("No edge with IP %s found" % edge_ip)

usage_doc = """
Usage: create-edge.py <command> <args...>


NOTE: Before executing the provisioning script,

    Kindly ensure your SSH key is available in the edge. You may run this command to copy your SSH key to the edge.
    ssh-copy-id root@EDGE_IP

    If you are using dedicated disk cache(s), kindly ensure you have partitioned and formatted all disk cache(s) before proceeding to execute the provisioning script.

    If you are using SSD disks, kindly reserve 20% storage device unpartitioned for better long-term performance.


Commands
========

login       <api username> <api password>
            * Saves your API login credentials.

enumerate   * Lists your clouds, locations, and edge servers.

register    <edge_ip> <location_id> <diskspace> <portspeed> <marketplace> <streaming_edge>
            * Registers edge server using edges.create() API. An edge server
              name of Company-Country-City-IP is auto-generated.
            * Location ID: get your location id with the "enumerate" command first.
            * Diskspace: in GB units.
            * Port speed: in Mbps units.
            * Marketplace: "yes" to enrol into marketplace, "no" otherwise.
            * Streaming edge: "yes" for streaming edge, "no" for normal edge

mount       <edge_ip> <block_device_name> <storage_bay_no>
            * SSH to the edge server and formats/mounts a partition.
            * Block device name format: /dev/xxx1
            * If the device has already mounted, unmount and remove from fstab before running this script.
            * Device will be mounted unto /mnt/nginx/bay-<storage_bay_no>
            * The assignment of storage bay number enable us to replace disk cache easily.

install     <edge_ip>
            * SSH to the edge server and runs installation commands. Requires
              registration and mounting to be done first.
            * Don't forget to copy your ssh key first.

activate    <edge_ip>
            * Activates edge server (INSTALLING -> ACTIVE).

enrol       <edge_ip>
            * Enrols the edge server into the marketplace.

withdraw    <edge_ip>
            * Withdraws the edge server from the marketplace.

speed       <edge_ip> <port_speed>
            * Changes port speed (in Mbps). This is used by the DNS load balancer.

delete      <edge_ip>
            * Deletes an edge server. USE WITH CAUTION.

pause       <edge_ip>
            * Pauses an edge server.
"""

mount_commands = """
mkdir -p /mnt/nginx/bay-{bay_no}

cat >> /etc/fstab <<EOF
# Added by onapp installation script
LABEL=bay-{bay_no} /mnt/nginx/bay-{bay_no} ext4 noatime,barrier=0 0 0
EOF

mount /mnt/nginx/bay-{bay_no}

mount
"""

install_commands = """
wget --quiet -O - https://api.onappcdn.com/install.sh | bash -s {edge_id} edge omega
"""

if __name__ == "__main__":
    main(sys.argv[1:])
