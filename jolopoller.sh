#!/usr/bin/env bash

#=================CONFIGURATION===================
DATASET_FILE="/vagrant/tmp/jolokia/jolometrics.txt"
EXISTING_TOMCAT_APPS="$(find /data/tomcat -maxdepth 1 |cut -d'/' -f4|grep -v '^$')"
DEFAULTS_PATH="/etc/default"
METRICS_PATH="/var/tmp"
SCRIPT_PWD="$(pwd)"
HEADER="Content-Type: application/json"
DATASET=$(cat $DATASET_FILE |egrep -v "^$|#"|sed -e 's|JVMINFO = ||g;s|\[||g;s|\]||g;s|\x27||g'|while read -r line; do printf "$line"; done)
CURL_TIMEOUTS=(3 3)
#=================CONFIGURATION===================

[[ -f $(which jq) ]] || { wget https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64 -O /usr/bin/jq && chmod +x /usr/bin/jq; }
[[ -f $METRICS_PATH/jolopoller.py ]] || find / -name "jolopoller.py" -exec /bin/cp -Rf {} $METRICS_PATH/jolopoller.py \;
[[ -f $METRICS_PATH/jolometrics.txt ]] || find / -name "jolometrics.txt" -exec /bin/cp -Rf {} $METRICS_PATH/jolometrics.txt \;

jq_arg='('.[]\|\(.request\|.mbean+.attribute+.path\),.value')'

jq_p()
  {
    # local IFS=''
    # curl -s --connect-timeout ${CURL_TIMEOUTS[0]} --max-time ${CURL_TIMEOUTS[1]} -H "$HEADER" -X POST -d '['$DATASET']' --url "http://127.0.0.1:$1/jolokia/"| jq $jq_arg ;
    /usr/bin/python $METRICS_PATH/jolopoller.py $1
  }

trunc()
  {
    local input="$@"
    { echo "0" > "$input" ; } 2>/dev/null
  }

jpoller()
  {
    if [[ "$#" -eq 1 ]]; then
      local TOMCAT_APPS="$@"
      local CHECKED_TOMCAT_APPS="$(echo $EXISTING_TOMCAT_APPS|xargs printf "%s""\n"|grep $TOMCAT_APPS)"
      if [[ ! -z "$CHECKED_TOMCAT_APPS" ]]; then
        for app_name in $CHECKED_TOMCAT_APPS; do
          joport="$(grep 'jolokia-jvm' $DEFAULTS_PATH/$app_name |sed -e 's/.*port=//g;s/\,.*//g')"
          [[ -d $METRICS_PATH/$app_name ]] || mkdir -p $METRICS_PATH/$app_name
          cd $METRICS_PATH/$app_name
          response=$(jq_p $joport)
          response_formatted=$(printf "$response""\n"|grep type=|sed -e 's/[[:space:]]/_/g')
          if [[ ! -z "$response" ]]; then
            for mbean_attr in $response_formatted; do
              mbean_attr_name=$(printf "$mbean_attr""\n"|sed -e 's|\/||g;s|"||g;s|=||g;s|[[:space:]]|_|g;s/,//g;s/type//g;s/name//g'|cut -d':' -f2);
              printf "$response""\n" |grep -A1 "$(echo $mbean_attr|sed -e 's/_/ /g')" |awk '/type=/{getline; print}' > "$mbean_attr_name";
            done
            printf "OK""\n";
            return 0;
          else
            printf "\033[0;31mERROR!!!\033[0m agent is unavailable""\n";
            return 1;
          fi
        done
      else
        printf "\033[0;31mERROR!!!\033[0m Application does not exist""\n";
        return 1;
      fi
    else
      printf "\033[0;31mERROR!!!\033[0m incorrect number of arguments""\n";
      return 1;
    fi
  }

jgetter()
  {
    if [[ "$#" -eq 2 ]]; then
      result="$(find $METRICS_PATH/$1 -name "*$2*" -exec cat {} \;)"
      path_to_result="$(find $METRICS_PATH/$1 -name "*$2*")"
      echo $result
      trunc $path_to_result
    else
      printf "\033[0;31mERROR!!!\033[0m incorrect number of arguments""\n";
      return 1;
    fi
  }

jlister()
  {
    if [[ "$#" -eq 1 ]]; then
      local TOMCAT_APPS="$@"
      for app_name in $TOMCAT_APPS; do
        joport="$(grep 'jolokia-jvm' $DEFAULTS_PATH/$app_name |sed -e 's/.*port=//g;s/\,.*//g')"
        [[ ! -z "$joport" ]] && curl -s -H "$HEADER" -X POST -d '[{"type": "list", "path":""},]' --url "http://127.0.0.1:$joport/jolokia/" | jq '.'
      done
    else
      printf "\033[0;31mERROR!!!\033[0m incorrect number of arguments""\n";
      return 1;
    fi
  }

jdiscovery()
  {
    l=$(ls /data/tomcat/);
    echo -n '{"data":['; for i in ${l}; do echo -n "{\"{#APP_NAME}\": \"$i\"},";done|sed -e 's:\},$:\}:';echo -n ']}';
  }

case $@ in
        "jpol"*)
          jpoller $(echo $@|sed -e 's/jpol//g');;
        "jlist"*)
          jlister $(echo $@|sed -e's/jlist//g');;
        "jget"*)
          jgetter $(echo $@|sed -e 's/jget//g');;
        "jdiscovery")
          jdiscovery;;
        *)
        printf "\033[0;33mUSAGE:\033[0m bash $0 jpol app_name | jget app_name app_metric | jlist app_name""\n";;
esac
