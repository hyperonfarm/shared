#!/bin/sh

host_type=`echo hypervisors`;
msqlh=`grep host /onapp/interface/config/database.yml | cut -d ":" -f2 | uniq | sed "s/[',  ]//g"`; 
msqlp=`grep password /onapp/interface/config/database.yml | cut -d ":" -f2 | uniq | sed "s/[',  ]//g"`;

mysqlexec () { mysql -h"$msqlh" -uroot -p"$msqlp" "$@" ; }

sssh () {
		ssh_exec="$(which ssh)"
		key_file="/onapp/interface/config/keys/private"
		test -f $key_file && $ssh_exec -qo PasswordAuthentication=no -o StrictHostKeyChecking=no $@ || $ssh_exec -i $key_file $@
	}

hv_group_ids=$(mysqlexec onapp -e "select distinct hypervisor_group_id from hypervisors where custom_config is not NULL\G" | grep -v "*" | cut -d":" -f2 | sed "s/[ ]//g";)

initialization () {

mysqlexec onapp -e "update virtual_machines set=NULL where identifier='$vm_identifier'"

}

