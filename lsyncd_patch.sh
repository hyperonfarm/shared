#!/bin/bash

LSNCDCFG="/etc/lsyncd.conf"
HOSTNAMES=$(crm_mon -r1|grep Online |sed -e "s/.*\[\(.*\)\].*/\1/g"|xargs echo)

crm resource unmanage lsyncd-cluster;

cat > /tmp/lsync.patch << EOF
sed -i '/delay =/d' $LSNCDCFG
sed -i '/update =/d' $LSNCDCFG
sed -i 's|.*"\/onapp\/interface\/log\/transactions\/".*|\tdelay = 60,\n&|' $LSNCDCFG
sed -i 's|.*"mkdir -p \/onapp\/interface\/log\/transactions\/  && rsync".*|\t    update = true,\n&|' $LSNCDCFG
EOF

for i in $HOSTNAMES; do

cat /tmp/lsync.patch | ssh root@$i "cat > /tmp/lsync.patch"

done

for i in $HOSTNAMES; do

ssh root@$i "cp /etc/lsyncd.conf{,.bak}; chattr +i /etc/lsyncd.conf.bak; service lsyncd stop; bash /tmp/lsync.patch;rm -f /tmp/lsync.patch; service lsyncd start"

done

crm resource manage lsyncd-cluster;
crm resource cleanup lsyncd-cluster;
