#!/usr/bin/env bash

APPS="/data/tomcat"
DEFAULTS_PATH="/etc/default"
JOPORT="13501"
JOHOST="127.0.0.1"

find_apps () {
  find $APPS -maxdepth 1 |cut -d'/' -f4|grep -v "^$"
}

TOMCAT_APPS=$(find_apps)

for tapp in $TOMCAT_APPS; do
  sed -i '/jolokia/d' $DEFAULTS_PATH/$tapp
  echo "JAVA_OPTS=\"\$JAVA_OPTS -javaagent:/usr/local/llnw/zabbix/bin/jolokia-jvm.jar=port=$JOPORT,host=$JOHOST\"" >> $DEFAULTS_PATH/$tapp
  JOPORT=$(( $JOPORT + 1 ))
done
