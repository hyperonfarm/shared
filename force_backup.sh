#!/bin/bash

#######FOR_ADMIN_USE_ONLY#########
# API_user and API_pass should be filled out
# Can be executed with "bash force_backup.sh <vm_identifier> <backup_server_id>" or with chmod +x force_backup.sh etc.
# Run bash force_backup.sh to see help
##################################

API_user=""
API_pass=""

PROCESS_COUNT="$(ps aux|grep force_backup.sh |grep -v grep| wc -l)"

[[ "$PROCESS_COUNT" > "2" ]] && { printf "\nProcess $0 is currently running;\n\nONLY ONE PROCESS CAN BE RAN AT THE SAME TIME !!!\n\n"; exit 1; }

COUNT_ARGS="$(echo $@|sed -e 's/ /\n/g'|wc -l)"

[[ "$COUNT_ARGS" != "2" ]] && { printf "\nUsage: bash $0 <vm_identifier> <bk_server_id>\n\n"; exit 1; }

INCREMENTAL_ENABLED="$(grep -i incremental /onapp/interface/config/on_app.yml|cut -d' ' -f2)"

msqlh=`grep host /onapp/interface/config/database.yml | cut -d ":" -f2 | uniq | sed "s/[',  ]//g"`;
msqlp=`grep password /onapp/interface/config/database.yml | cut -d ":" -f2 | uniq | sed "s/[',  ]//g"`;

VM_IDENTIFIER="$1"
BK_SERVER="$2"

pipe="/tmp/pipe_$VM_IDENTIFIER"

LOG_FILE="/var/log/force_backup.log"

if [[ ! -p $pipe ]]; then
    mkfifo $pipe
fi

if [[ ! -f $LOG_FILE ]]; then
    touch $LOG_FILE
fi

get_vm_id ()
		{
		mysql -h$msqlh -uroot -p$msqlp onapp -Bse 'select id from virtual_machines where identifier in ("'$1'");'
		}

get_bk_tr_id ()
		{
		mysql -h$msqlh -uroot -p$msqlp onapp -Bse 'select id from transactions where status="pending" and action in ("take_backup","take_incremental_backup") and identifier in ('$1');'
		}

exec_curl ()
		{
		curl -s --insecure -i -X POST -H 'Accept: application/json' -H 'Content-type:application/json' -u "$API_user:$API_pass" --url https://127.0.0.1/virtual_machines/$1/backups.json -d '{"backup":{"note":""}}' | tee -a $LOG_FILE
		}

extract_bk_identifier ()
		{
		echo "$@" |sed -e 's/:/\n/g;'|grep identifier -A1|cut -d',' -f1|awk 'NR==2'
		}

disable_triaged_bses ()
		{
		mysql -h$msqlh -uroot -p$msqlp onapp -Bse 'update backup_servers set enabled=0 where id not in ("'$1'");'
		}

enable_triaged_bses ()
		{
		mysql -h$msqlh -uroot -p$msqlp onapp -Bse 'update backup_servers set enabled=1 where id not in ("'$1'");'
		}

task_runner ()
		{
		su - onapp -c 'cd /onapp/interface; RAILS_ENV=production rails runner -e production "_date=DateTime.now; puts _date; def quit() puts :quit end; puts quit; STDOUT.flush; Transaction.find('$1').run" 2>/dev/null'
		}

change_bk_type ()
		{
		mysql -h$msqlh -uroot -p$msqlp onapp -Bse 'update backups set initiated="days" where identifier='$1' limit 1;'
		}

global_exec ()
		{

if [[ "$INCREMENTAL_ENABLED" == "true" ]];
then
	
	INLOOP_VM_ID="$(get_vm_id $VM_IDENTIFIER)"
	
	EXECUTION="$(exec_curl $INLOOP_VM_ID)"
	
	INLOOP_BK_TR_IDENT="$(extract_bk_identifier $EXECUTION)"
	
	INLOOP_BK_TR_ID="$(get_bk_tr_id $INLOOP_BK_TR_IDENT)"
	
	disable_triaged_bses $BK_SERVER

	change_bk_type $INLOOP_BK_TR_IDENT
	
	{ task_runner $INLOOP_BK_TR_ID >$pipe ; } &
	
	while true
		do
    		if read line <$pipe; then
        		if [[ "$line" == 'quit' ]]; then
			sleep 5
			enable_triaged_bses $BK_SERVER
            		break
        		fi
    		fi
	done
	
	rm $pipe

fi
		}

{ global_exec ; } &
