#!/bin/sh

host_type=`echo hypervisors`;
msqlh=`grep host /onapp/interface/config/database.yml | cut -d ":" -f2 | uniq | sed "s/[',  ]//g"`; 
msqlp=`grep password /onapp/interface/config/database.yml | cut -d ":" -f2 | uniq | sed "s/[',  ]//g"`;

mysqlexec () { mysql -h"$msqlh" -uroot -p"$msqlp" "$@" ; }
colorize () { }

list_ips=$(mysqlexec onapp -e "select ip_address from $host_type\G" | grep -v "*" | cut -d":" -f2 | sed "s/[ ]//g")

for i in $list_ips; do 
	_node_identifier=$(ssh -i /onapp/interface/config/keys/private -o StrictHostKeyChecking=no $i "onappstore getid|sed -e 's|.*uuid=||g;s|[[:space:]].*||g'")
	echo;
	mysqlexec onapp -e "select label from $host_type where ip_address='$i'\G" | grep -v "*" | cut -d":" -f2 | sed "s/[ ]//g"; 
	echo; 
	echo $i; 
	echo; 
	curl -s -X PUT $i:8080/is/Node/$_node_identifier -d '{"state":3}'|ruby -rjson -e 'print JSON.pretty_generate(JSON.parse(STDIN.read))'; 
done
