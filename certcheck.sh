#!/usr/bin/env bash

date_from_cert=$(openssl x509 -in /etc/letsencrypt/live/pumpdumpwars.com/fullchain* -text -noout|grep After|sed -s 's/.*After*.//g;s/: //')
date_now=$(date +%s)
date_end=$(date --date="$date_from_cert" +%s)
seconds_left=$(( $date_end - $date_now ))
days_left=$(( $seconds_left / 3600 / 24 ))

if [[ "$days_left" -lt "14" ]]; then
  certbot renew >> /var/log/certbot.log
else
  echo "There's still $days_left for $1" >> /var/log/certbit.log
fi
