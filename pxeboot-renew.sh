#!/bin/bash

input_length="$(echo $1 | awk '{ print length;}')"

centos_version_from="centos6"
centos_version_to="centos7"

if [[ "$input_length" -lt 10 ]]; then
	centos_version_from=${1:-""}
	centos_version_to=${2:-""}
fi

runn () {
	#renewal line which will substitute the necessary lines
	sed -i "s|images\/$centos_version_from\/ramdisk-kvm\/vmlinuz|images\/$centos_version_to\/ramdisk-kvm\/vmlinuz|g;s|images\/$centos_version_from\/ramdisk-kvm\/initrd.img|images\/$centos_version_to\/ramdisk-kvm\/initrd.img|g" "$@"

}


for config in "$@"; do

	fullpath=$(find /tftpboot/pxelinux.cfg/ -name "*$config")

	runn $fullpath

done
