#!/bin/sh

host_type=`echo hypervisors`;
msqlh=`grep host /onapp/interface/config/database.yml | cut -d ":" -f2 | uniq | sed "s/[',  ]//g"`; 
msqlp=`grep password /onapp/interface/config/database.yml | cut -d ":" -f2 | uniq | sed "s/[',  ]//g"`;

mysqlexec () { mysql -h"$msqlh" -uroot -p"$msqlp" "$@" ; }

sssh () {
		ssh_exec="$(which ssh)"
		key_file="/onapp/interface/config/keys/private"
		test -f $key_file && $ssh_exec -qo PasswordAuthentication=no -o StrictHostKeyChecking=no $@ || $ssh_exec -i $key_file $@
	}

hv_group_ids=$(mysqlexec onapp -e "select distinct hypervisor_group_id from hypervisors where custom_config is not NULL\G" | grep -v "*" | cut -d":" -f2 | sed "s/[ ]//g";)

initialization () {

for hvg in $hv_group_ids; do 
		list_ips=$(mysqlexec onapp -e "select ip_address from $host_type where hypervisor_group_id=$hvg and ip_address is not NULL\G" | grep -v "*" | cut -d":" -f2 | sed "s/[ ]//g")
			for ipd in $list_ips; do 
				sssh $ipd "for i in \$(getdegradedvdisks |grep degraded_vdisks|sed -e 's|.*(||g;s|).*||g;s|,| |g'|xargs echo); do onappstore $@ uuid=\$i 2>/dev/null; done" && break
				continue
				done
done
}

case "$1" in
	repair)
	initialization repair
	;;
	synchstatus)
	initialization resynchstatus
	;;
	*)
	echo "Usage: auto_heal repair/synchstatus"
	;;
esac
