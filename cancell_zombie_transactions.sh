#!/bin/bash

DAYS_FOR_ZOMBIE="$1"

API_user=""
API_pass=""

CANCELL_LOG="/var/log/cancelled_transactions.log"

msqlh=`grep host /onapp/interface/config/database.yml | cut -d ":" -f2 | uniq | sed "s/[',  ]//g"`;
msqlp=`grep password /onapp/interface/config/database.yml | cut -d ":" -f2 | uniq | sed "s/[',  ]//g"`;

BACKUP_SERVERS_IPS="$(mysql -h$msqlh -uroot -p$msqlp onapp -Bse "select ip_address from backup_servers;"|xargs echo|sed -e 's/[[:space:]]/|/g')"

TRANSACTIONS_IDS="$(mysql onapp -Bse "select id from transactions where status='running' and action in ('destroy_backup','take_backup','take_incremental_backup');"|xargs echo)"

LOGS_PATHS="$(for id in eval $TRANSACTIONS_IDS; do find /onapp/interface/log/transactions/ -name "*$id*"; done)"


sssh () {
                ssh_exec="$(which ssh)"
                key_file="/onapp/interface/config/keys/private"
                test -f $key_file && $ssh_exec -qo PasswordAuthentication=no $@ || $ssh_exec -i $key_file $@
        }

get_identifier () {
                mysql -h$msqlh -uroot -p$msqlp onapp -Bse 'select identifier from transactions where id='$1';'
                  }

get_action () {
                mysql -h$msqlh -uroot -p$msqlp onapp -Bse 'select action from transactions where id='$1';'
              }

get_log_item () {
                mysql -h$msqlh -uroot -p$msqlp onapp -Bse 'select id from log_items where target_id='$1';'
                }

get_days_running () {
                mysql -h$msqlh -uroot -p$msqlp onapp -Bse 'select timestampdiff(DAY,updated_at,NOW()) from transactions where id='$1';'
                    }

for ID in $LOGS_PATHS;
        do
        INLOOP_BS_IP="$(grep "Remote Server" $ID | sort -u|sort -u|cut -d' ' -f3|egrep "$BACKUP_SERVERS_IPS";)"
        INLOOP_TR_ID="$(echo "$ID" | cut -d'/' -f9|sed -e 's|\.log*.||g')"
        INLOOP_TR_ACTION="$(get_action $INLOOP_TR_ID)"
        INLOOP_TR_LAPSE="$(get_days_running $INLOOP_TR_ID)"
        INLOOP_LOG_ITEM="$(get_log_item $INLOOP_TR_ID)"
        BK_IDENTIFIER="$(get_identifier $INLOOP_TR_ID)"


        if [[ "$(sssh $INLOOP_BS_IP "ps aux|grep $BK_IDENTIFIER|grep -v grep|sort -u|wc -l;")" == "0" ]];
        then
                if [[ "$INLOOP_TR_LAPSE" > "$DAYS_FOR_ZOMBIE" ]];
                then

                printf "$(date +%F-%H-%M-%S)==========================================================\n" >> $CANCELL_LOG
                echo "BS: $INLOOP_BS_IP; Transaction: $INLOOP_TR_ID; Action: $INLOOP_TR_ACTION; Log_item: $INLOOP_LOG_ITEM; Running: $INLOOP_TR_LAPSE days;" >> $CANCELL_LOG
                echo '' >> $CANCELL_LOG
                grep "$BK_IDENTIFIER" $ID |grep -v error| grep -v "I/O" | grep -v "vanished" >> $CANCELL_LOG
                echo '' >> $CANCELL_LOG
                sssh $INLOOP_BS_IP "ps aux|grep $BK_IDENTIFIER|grep -v grep|sort -u;" >> $CANCELL_LOG
                echo "CANCELLED" >> $CANCELL_LOG
                echo '' >> $CANCELL_LOG
                echo "ls -la /onapp/templates/backups/${BK_IDENTIFIER:0:1}/${BK_IDENTIFIER:1:1}/" >> $CANCELL_LOG
                sssh $INLOOP_BS_IP "ls -la /onapp/templates/backups/${BK_IDENTIFIER:0:1}/${BK_IDENTIFIER:1:1}/ 2>/dev/null|grep $BK_IDENTIFIER" >> $CANCELL_LOG
                printf "==========================================================\n" >> $CANCELL_LOG

                curl -s --insecure -i -X POST -u "$API_user:$API_pass" -H 'Accept: application/json' -H 'Content-type: application/json' https://127.0.0.1/transactions/$INLOOP_TR_ID/cancel_zombie

                fi
        fi

done
