#!/usr/bin/env python

import json
import requests
import re
import os
import ast
import argparse
import yaml
import pwd
import grp

#READ THE CONFIGURATION FILE, MUST PRODUCE A DICTIONARY
def read_conf():
    pwd = os.path.dirname(os.path.realpath(__file__))
    cfg_subdir = 'jo_poller_cfg'
    for filename in os.listdir(pwd + '/' + cfg_subdir):
        if re.search('jo_poller', filename):
            if re.search('env.yml', filename):
                yaml_path = os.path.join(pwd,cfg_subdir,filename)
                break
            else:
                yaml_path = os.path.join(pwd,cfg_subdir,'jo_poller.yml')
    with open(yaml_path,'r') as ymlcfg:
        yaml_cfg = yaml.load(ymlcfg)
    return yaml_cfg
##################

#FUNCTION TO GET MULTIPLE APP LOCATIONS RETURNS A LIST OF APPLICATIONS
def list_apps(in_apps):
    apps = []
    if not isinstance(in_apps, basestring):
        for dir_app in in_apps:
            if os.path.exists(dir_app):
                apps += os.listdir(dir_app)
    elif isinstance(in_apps, basestring):
        apps.append(in_apps)
    for ex in apps:
        if ex in yaml['excluded_apps']:
            apps.remove(ex)
    return apps
##################

#FUNCTION TO GET THE JOLOKIA PORT ACCORDING TO APP NAME RETURNS PORT NUMBER IN STRING
def find_port(app_name):
    config = open(yaml['java_default'] + "/" + app_name,"r")
    match_lines = []
    ##PROCEDURE FOR RETREIVING JOLOKIA CONFIG PARAM WITH PORT
    for line in config.readlines():
        if re.search("jolokia-jvm.jar",line):
            match_lines.append(line.strip())
    config.close()
    ##PROCEDURE FOR RETREIVING WORD PORT AND VALUE NEXT TO IT RETURNS NONE IF NOT FOUND
    try:
        for tmp_st1 in match_lines[0].split(','):
            if re.search('port',tmp_st1):
                count = 0
                for tmp_st2 in tmp_st1.split('='):
                    if 'port' == tmp_st2:
                        return tmp_st1.split('=')[count+1]
                    count += 1
    except Exception:
        return None
##################

#FUNCTION GETS PATH TO FILE AND WRITES VALUE INTO IT
def file_io(path,filename,value,mode):
    if mode == 'r':
        file = open(os.path.join(path,filename),mode)
        print file.readlines()[0].strip()
        file.close()
    elif mode == 'w':
        uid = pwd.getpwnam(yaml['metrics_consumer']).pw_uid
        gid = grp.getgrnam(yaml['metrics_consumer']).gr_gid
        file = open(os.path.join(path,filename),mode)
        file.write(value)
        file.close()
        os.chown(path + '/' + filename,uid,gid)
        os.chmod(path + '/' + filename, 0o644)
##################

#FUNCTION TAKES JSON AND RETURNS A ONE DIMENSIONAL DICTIONARY WITH MBEAN-VALUE PAIRS
def json_strip(json_struct):
    mbean_name_value = {}
    sorted_dataset = parse_config(yaml['jolokia_data'])
    mbean_roots = []
    for dict in sorted_dataset:
        mbean_roots.append(dict['mbean'].split(':')[0])
    sort_uniq_mbean_roots = sorted(set(mbean_roots))
    for dict in json_struct:
        if dict['status'] == 200:
            _regexp = ['type=','name=',',',':'] + sort_uniq_mbean_roots
            mbean = re.sub(r'|'.join(map(re.escape,_regexp)),'',str(dict['request']['mbean']))
            attribute = str(dict['request']['attribute'])
            if 'path' in dict['request'] and 'value' in dict:
                path = str(dict['request']['path'])
                full_mbean = re.sub(r'\s+','_',mbean + attribute + path)
                value = str(dict['value'])
                mbean_name_value.update({full_mbean:value})
            elif 'value' in dict:
                full_mbean = re.sub(r'\s+','_',mbean + attribute)
                value = str(dict['value'])
                mbean_name_value.update({full_mbean:value})
    return mbean_name_value
##################

#FUNCTION PARSES CONFIG FILE AND RETURNS A SORTED LIST OF DICTIONARIES THAT CAN BE PASSED TO REQUEST
def parse_config(jo_dataset):
    dataset = []
    for line in jo_dataset.splitlines():
        if re.search(r'#',line) is None:
            tmp_line = re.sub(r'\]\'','',re.sub(r'\'\[','', re.sub(r'JVMINFO =','',line).strip())).strip(',')
            if not not tmp_line:
                dataset.append(ast.literal_eval(tmp_line))
    return dataset
##################

#FUNCTION CREATES FOLDERS FOR EACH APP AND STORES
def metrics_storage(path,app_name):
    if not os.path.exists(path):
        uid = pwd.getpwnam(yaml['metrics_consumer']).pw_uid
        gid = grp.getgrnam(yaml['metrics_consumer']).gr_gid
        os.makedirs(path)
        os.chown(path, uid, gid)
        os.chmod(path, 0o755)
    if app_name in list_apps(yaml['existing_apps']):
        if not os.path.exists(path + '/' + app_name):
                uid = pwd.getpwnam(yaml['metrics_consumer']).pw_uid
                gid = grp.getgrnam(yaml['metrics_consumer']).gr_gid
                os.makedirs(path + '/' + app_name)
                os.chown(path + '/' + app_name,uid,gid)
                os.chmod(path + '/' + app_name, 0o755)
##################

#FUNCTION POLLS THE JOLOKIA FRONTEND AND GETS JSON OBJECT STRUCTURE WITH ALL METRICS IN CONFIG
def jolo_to_json(port,in_timeout):
    sorted_dataset = parse_config(yaml['jolokia_data'])
    url = 'http://127.0.0.1:{0}/jolokia/'.format(port)
    headers = {'content-type': 'application/json'}
    r = requests.post(url, data=json.dumps(sorted_dataset), headers=headers, timeout=in_timeout)
    return json.loads(r.text)
##################

#FUNCTION THAT DISCOVERS THE APPS
def get_discovery_items(apps_folder):
    try:
        tomcat_dir = list_apps(apps_folder)
        apps = []
        data = {}
        for dir in tomcat_dir:
            apps.append({'{#APP_NAME}':dir})
        data.update({'data':apps})
        print json.dumps(data, indent=4)
    except Exception:
        pass
##################

#MIDDLEWARE FUNCTION THAT STORES METRICS ON DISK
def store_metrics(app_name):
    try:
        port = find_port(app_name)
        metrics_storage(yaml['metrics_root_path'],app_name)
        received_metrics=json_strip(jolo_to_json(port,yaml['request_timeout']))
        for mbean in received_metrics:
            file_io(yaml['metrics_root_path'] + '/' + app_name,mbean,received_metrics[mbean]+"\n","w")
        print "OK"
    except Exception:
        print "ERROR!!!"
##################

#FUNCTION GETS AND PRINTS STORED METRIC
def get_metrics(path,app_name,metric_name):
    filename = app_name + "/" + metric_name
    if os.path.isfile(path + '/' + filename):
        file_io(path,filename,"","r")
        file_io(path,filename,"0"+"\n","w")
    # else:
    #     print "\nNo such metric. Please check the list of available metrics:\n"
    #     print sorted(os.listdir(path + "/" + app_name))
    #     print "\n"
##################

#FUNCTION THAT GENERATES ZABBIX USERPARAMS ACCORDING TO YAML CONFIGURATION
def userparams_gen(userparams_path):
    try:
        file = open(os.path.join(userparams_path),"w")
        script = os.path.dirname(os.path.abspath(__file__)) + "/" + os.path.basename(__file__)
        up = "UserParameter="
        sorted_dataset = parse_config(yaml['jolokia_data'])
        mbean_name_full = []
        discovery_param = ["apps.discovery","discovery"]
        poldata_param = ["PolData","jpol"]
        mbean_roots = []
        for dict in sorted_dataset:
            mbean_roots.append(dict['mbean'].split(':')[0])
        sort_uniq_mbean_roots = sorted(set(mbean_roots))
        for dict in sorted_dataset:
            _regexp = [':','type=',','] + sort_uniq_mbean_roots
            mbean_re = re.sub(r'|'.join(map(re.escape,_regexp)),'',str(dict['mbean'])).split('name=')
            mbean = mbean_re[-1] + mbean_re[0] if mbean_re[-1] != mbean_re[0] else mbean_re[0]
            attribute = str(dict['attribute'])
            if 'path' in dict:
                path = str(dict['path'])
                full_mbean = re.sub(r'\s+','_',mbean + attribute + path)
                mbean_name_full.append(full_mbean)
            else:
                full_mbean = re.sub(r'\s+','_',mbean + attribute)
                mbean_name_full.append(full_mbean)
        disc_full_userparam = up + discovery_param[0] + ",python" + " " + script + " " + discovery_param[1] + "\n"
        pol_full_userparam = up + poldata_param[0] + "[*]" + ",python" + " " + script + " " + poldata_param[1] + " " + "$1" + "\n"
        file.write(disc_full_userparam)
        file.write(pol_full_userparam)
        for mb in mbean_name_full:
            userparam = up + mb + "[*]" + ",python" + " " + script + " " + "jget" + " " + "$1" + " " + "$2" + "\n"
            file.write(userparam)
        file.close()
        print "Userparams generated OK"
    except Exception:
        print "Userparams generate ERROR"
##################

#JOLOKIA AGENT DEPLOY FUNCTION READS DEFAULT CONFIGS AND WRITES THEM WITH ADDITIONAL JVM CONFIG
def jolokia_deploy(host,first_port,jvm_agent,java_configs,apps_dirs):
    if os.path.exists(jvm_agent):
        for app in list_apps(apps_dirs):
            file_path = java_configs + "/" + app
            if os.path.isfile(os.path.join(java_configs,app)):
                if not find_port(app):
                    app_port_exist = []
                    first_port = yaml['jvm_agent_start_port']
                    for app_i in list_apps(yaml['existing_apps']):
                        if find_port(app_i):
                            app_port_exist.append(int(find_port(app_i)))
                    sorted_exist_ports = sorted(set(app_port_exist))
                    while int(first_port) in sorted_exist_ports:
                        first_port += 1
                elif find_port(app):
                    first_port = int(find_port(app))
                add_config_param = "JAVA_OPTS=\"$JAVA_OPTS -javaagent:{}=port={},host={}\"".format(jvm_agent,first_port,host) + "\n"
                with open(file_path,"r") as file:
                    lines_orig = file.readlines()
                count_lines = 0
                for line in lines_orig:
                    count_lines += 1
                with open(file_path,"a") as file:
                    ln = 0
                    for line in lines_orig:
                        ln += 1
                        if re.search("jolokia-jvm.jar",line) is None and ln == count_lines:
                            file.write(add_config_param)
                            print "{} JVM agent deploy OK".format(app)
                        elif re.search("jolokia-jvm.jar",line) and ln == count_lines:
                            print "JVM agent is already deployed on {}".format(app)
            else:
                print "{} APP DOES NOT EXIST!".format(app)
            metrics_storage(yaml['metrics_root_path'],app)
    else:
        print "\nError!!! {} does not exist!\n".format(jvm_agent)
##################

#FUNCTION FOR DISPLAYING HELP
def print_help():
    print "\nPlease provide arguments." + "\n"
    print "Usage: python {} [jpol|jget|discovery|userparam_gen|jolokia_agent_deploy]".format(os.path.realpath(__file__)) + "\n"
    print "*jpol <app_name>" + "\n"
    print "*jget <app_name> <metric_name>" + "\n"
    print "*jolokia_agent_deploy (optional)<app_name>(/optional)" + "\n"
    print "*discovery" + "\n"
    print "*userparam_gen" + "\n"
##################

#FUNCTION RECEIVES COMMAND LINE ARGUMENTS AND PASSES THEM TO OTHER FUNCTIONS IN CASE STYLE
def parse_input():
    parser = argparse.ArgumentParser(description='Script for polling jolokia and passing data to zabbix agent')
    parser.add_argument('nums', nargs='*')
    args = parser.parse_args()
    if args.nums:
        if len(args.nums) == 2:
            if args.nums[0] == 'jpol':
                if args.nums[1] in list_apps(yaml['existing_apps']):
                    store_metrics(args.nums[1])
                else:
                    print "\nAPP NAME DOES NOT EXIST!\n"
                    print_help()
            elif args.nums[0] == 'jolokia_agent_deploy' and args.nums[1] in list_apps(yaml['existing_apps']):
                jolokia_deploy(yaml['jvm_agent_host'],yaml['jvm_agent_start_port'],yaml['jvm_agent_file'],yaml['java_default'],args.nums[1])
            else:
                print "\nNO SUCH ARGUMENT OR WRONG NUMBER OF ARGUMENTS!\n"
                print_help()
        elif len(args.nums) == 3:
            if args.nums[0] == 'jget':
                if args.nums[1] in list_apps(yaml['existing_apps']):
                    get_metrics(yaml['metrics_root_path'],args.nums[1],args.nums[2])
                else:
                    print "\nAPP NAME DOES NOT EXIST!\n"
                    print_help()
            else:
                print "\nNO SUCH ARGUMENT!\n"
                print_help()
        elif len(args.nums) == 1:
            if args.nums[0] == 'discovery':
                get_discovery_items(yaml['existing_apps'])
            elif args.nums[0] == 'userparam_gen':
                userparams_gen(yaml['userparams'])
            elif args.nums[0] == 'jolokia_agent_deploy':
                jolokia_deploy(yaml['jvm_agent_host'],yaml['jvm_agent_start_port'],yaml['jvm_agent_file'],yaml['java_default'],yaml['existing_apps'])
            else:
                print "\nINCORRECT NUMBER OF ARGUMENTS OR WRONG FUNCTION!\n"
                print_help()
        else:
            print "\nTOO MUCH ARGUMENTS!\n"
            print_help()
    else:
        print_help()
##################

#MAIN FUNCTION
def main():
    #initialize systemwide configuration
    global yaml
    yaml = read_conf()
    #####
    parse_input()
##################

if __name__ == '__main__':
    main()
