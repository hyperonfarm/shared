#!/bin/sh
usage()
{
    echo >&2 "Usage: $0 [--rbthost RBT_HOST] [--vcdlogin VCD_LOGIN] [--vcdpasswd VCD_PASSWD] [--vcdvhost VCD_VHOST] [--rbtlogin RBT_LOGIN] [--rbtpasswd RBT_PASSWD] [--ha-enabled] [-h|--help]

                           --rbthost   RBT_HOST   : IP address/FQDN where RabbitMQ Server runs.
                                                    The RabbitMQ will be installed and configured on the current box if localhost/127.0.0.1 or box's public IP address (listed in SNMP_TRAP_IPS) is specified.
                                                    Default values is 127.0.0.1.

                              VCD_*      : Options are usefull if vCloud/RabbitMQ are already installed and configured.

                           --vcdlogin  VCD_LOGIN  : RabbitMQ/vCloud user. Default value is 'rbtvcd'.
                           --vcdpasswd VCD_PASSWD : RabbitMQ/vCloud user password. The random password is generated if isn't specified.
                           --vcdvhost  VCD_VHOST  : RabbitMQ/vCloud vhost. Default value is '/'

                              RBT_*         : Options are used to configure RabbitMQ manager account. If local RabbitMQ server only!

                           --rbtlogin  RBT_LOGIN  : RabbitMQ manager login. The default value is 'rbtmgr'.
                           --rbtpasswd RBT_PASSWD : RabbitMQ manager password. The random password is generated if isn't specified.

                           --ha-enabled : Use the option if Hight Availablity is enabled
                                          This will not start/restart RabbitMQ Server services
                           -h|--help    : print this info"
}

TEMP=`getopt -o h --long help,rbthost:,vcdlogin:,vcdpasswd:,vcdvhost:,rbtlogin:,rbtpasswd:,ha-enabled -n 'onapp-cp-rabbitmq.sh' -- "$@"`

if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi

# Note the quotes around $TEMP: they are essential!
eval set -- "$TEMP"

USAGE=0
HA_ENABLED=0
RBT_HOST=
RBT_HOST_DEFAULT=127.0.0.1
VCD_VHOST=
VCD_VHOST_DEFAULT=/
VCD_LOGIN=
VCD_LOGIN_DEFAULT=rbtvcd
RBT_LOGIN=
RBT_LOGIN_DEFAULT=rbtmgr
VCD_PASSWD=
RBT_PASSWD=
VCD_EXCHANGE=vcloud
while true; do
  case "$1" in
    -h | --help ) USAGE=1; shift ;;
    --ha-enabled ) HA_ENABLED=1; shift ;;
    --rbthost ) RBT_HOST="$2"; shift 2 ;;
    --vcdlogin ) VCD_LOGIN="$2"; shift 2 ;;
    --vcdpasswd ) VCD_PASSWD="$2"; shift 2 ;;
    --rbtlogin ) RBT_LOGIN="$2"; shift 2 ;;
    --rbtpasswd ) RBT_PASSWD="$2"; shift 2 ;;
    --vcdvhost ) VCD_VHOST="$2"; shift 2 ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

if [ $USAGE -eq 1 ]; then
    usage;
    exit 0
fi

manage_yml()
{

    local YAML=$1
    local ACTION=$2
    local PARAM=$3

    local VAL=$4

    local RES=

    if [ "x${ACTION}" = "x" ]; then
        echo "$RES"
        return 1
    fi

    if [ "x${ACTION}" = "xget" ]; then
        RES=`egrep -e "^[[:space:]]*$PARAM:.*" $YAML 2>/dev/null | sed "s#^[[:space:]]*$PARAM:[[:space:]]\+'\?##g;s#'\?[[:space:]]*##g" | sed "s#^[[:space:]]*$PARAM:[[:space:]]\+\"\?##g;s#\"\?[[:space:]]*##g"`
        echo "$RES"
        return 0
    fi

    if [ "x${ACTION}" = "xset" ]; then
        if egrep -e "^[[:space:]]*$PARAM:" $YAML >/dev/null 2>&1; then
            if egrep -e "^[[:space:]]*$PARAM:[[:space:]]+['|\"]*$VAL['|\"]*" $YAML >/dev/null 2>&1; then
                if [ "x${PARAM}" != "xpassword" ]; then log "The '$PARAM' value '$VAL' already in the $YAML"; else log "The '$PARAM' value 'XXXXXXXX' already in the $YAML"; fi
            else
                if [ "x${PARAM}" != "xpassword" ]; then log "Change the '$PARAM' value into '$VAL' in the $YAML"; else log "Change the '$PARAM' value into 'XXXXXXXX' in the $YAML"; fi
                VAL=`echo "$VAL" | sed -e 's/[\/&]/\\\&/g'`
                sed -i "s/^\([[:space:]]*\)$PARAM:[[:space:]]\+.\+/\1$PARAM: $VAL/g" $YAML
            fi
        else
            if [ "x${PARAM}" != "xpassword" ]; then log "Add '$PARAM' with value '$VAL' to the $YAML"; else log "Add '$PARAM' with value 'XXXXXXXX' to the $YAML"; fi
            echo "$PARAM: '$VAL'" >> ${YAML}
        fi
        return $?;
    fi

    unset YAML ACTION PARAM VAL RES
}

gen_random_passwd()
{
   tr -dc 'a-zA-Z0-9!%^&*()' < /dev/urandom | fold -w 10 | sed 5q |head -1
}

rabbitmq_check_user()
{
    local USER=$1

    if [ "x${USER}" = "x" ]; then
        log "rabbitmq_check_user(): undefined user name!"
        return 1
    fi

    sh -c "rabbitmqctl -q list_users 2>/dev/null | egrep -e '^$USER[[:space:]]+' >/dev/null 2>&1"

}

rabbitmq_check_vhost()
{
    local VHOST=$1

    if [ "x${VHOST}" = "x" ]; then
        log "rabbitmq_check_vhost(): undefined vhost name!"
        return 1
    fi

    sh -c "rabbitmqctl -q list_vhosts name 2>/dev/null | egrep -e '^$VHOST' >/dev/null 2>&1"

}

rabbitmq_configure_user()
{
    local LOGIN=$1
    local PASSWD=$2
    local VHOST=$3
    local PERM_CONFIGURE="$4"
    local PERM_READ="$5"
    local PERM_WRITE="$6"
    local PASSWD_CHANGE=$7
    local TAG=$8

    if [ "x${LOGIN}" != "x" -a "x${PASSWD}" != "x" ]; then
        rabbitmq_check_user "$LOGIN"
        if [ $? -eq 0 ]; then
            if [ $PASSWD_CHANGE -eq 1 ]; then
                log "Change password for the '$LOGIN' user"
                sh -c "rabbitmqctl -q change_password $LOGIN '$PASSWD'"
            fi
        else
            log "Add the '$LOGIN' user with the password XXXXXXXX"
            sh -c "rabbitmqctl -q add_user $LOGIN '$PASSWD'"
        fi
    fi
    if [ "x${LOGIN}" != "x" -a "x${TAG}" != "x" ]; then
        log "Tag the '$LOGIN' user as '$TAG'"
        rabbitmqctl -q set_user_tags $LOGIN $TAG
    fi
    if [ "x${LOGIN}" != "x" -a "x${VHOST}" != "x" ]; then
        rabbitmq_check_vhost "$VHOST"
        if [ $? -ne 0 ]; then
            log "Add the '$VHOST' vhost"
            rabbitmqctl -q add_vhost $VHOST
        fi

        if [ "x${PERM_CONFIGURE}" != "x" -a "x${PERM_READ}" != "x" -a "x${PERM_WRITE}" != "x" ]; then
            log "Set configure '${PERM_CONFIGURE}', read '${PERM_READ}', write '${PERM_WRITE}' permissions for the '$LOGIN' user on the '$VHOST' vhost"
            rabbitmqctl -q set_permissions -p $VHOST $LOGIN "${PERM_CONFIGURE}" "${PERM_READ}" "${PERM_WRITE}"
        fi
    fi
}

PWD=`pwd`
RUN_PATH=`dirname $0`

ONAPP_ROOT="/onapp"
ONAPP_DIR="$ONAPP_ROOT/interface"
ONAPP_RABBITMQ="$ONAPP_ROOT/onapp-rabbitmq"
RABBITMQ_MGR=$ONAPP_RABBITMQ/.rabbitmq.mgr
ONAPP_INSTALL="${ONAPP_ROOT}/onapp-install"
ONAPP_CP_CONF="${ONAPP_DIR}/config/on_app.yml"

logger "$0 - Started"

LOG='onapp-cp-rabbitmq.log'


if echo $RUN_PATH | egrep -e '^\/' >/dev/null 2>&1; then
    EXEC_PATH=$RUN_PATH
else
    EXEC_PATH=${PWD}/${RUN_PATH}
fi

if [ ! -f ${ONAPP_INSTALL}/onapp-install.functions ]; then
    logger "$0 - file not found ${ONAPP_INSTALL}/onapp-install.functions"
    exit 1
fi

source ${ONAPP_INSTALL}/onapp-install.functions

#       ---  MAIN ---     #

logger "$0 - Logging to $LOG"

log ""
log "`date '+%F %H:%M'` Start configuring RabbiMQ for Control Panel and vCloud" "SUCCESS"
logger "$0 - Start configuring RabbiMQ for Control Panel and vCloud"

if [ ! -f $ONAPP_CP_CONF ]; then
    log "OnApp configuration File $ONAPP_CP_CONF not found"
    exit 1
fi


RBT_HOST_CURRENT=
VCD_LOGIN_CURRENT=
VCD_PASSWD_CURRENT=
VCD_PASSWD_CHANGED=1
VCD_VHOST_CURRENT=
log "Get existed configuration of OnApp/vCloud connection to RabbitMQ from $ONAPP_CP_CONF"
RBT_HOST_CURRENT=`manage_yml "$ONAPP_CP_CONF" "get" "rabbitmq_host"`
VCD_LOGIN_CURRENT=`manage_yml "$ONAPP_CP_CONF" "get" "rabbitmq_login"`
VCD_PASSWD_CURRENT=`manage_yml "$ONAPP_CP_CONF" "get" "rabbitmq_password"`
VCD_VHOST_CURRENT=`manage_yml "$ONAPP_CP_CONF" "get" "rabbitmq_vhost"`

RBT_LOGIN_CURRENT=
RBT_PASSWD_CURRENT=
RBT_PASSWD_CHANGED=1
if [ -f $RABBITMQ_MGR ]; then
    log "Get existed configuration of RabbitMQ management account from $RABBITMQ_MGR"
    RBT_LOGIN_CURRENT=`cat $RABBITMQ_MGR 2>/dev/null | cut -d ' ' -f 1 2>/dev/null`
    RBT_PASSWD_CURRENT=`cat $RABBITMQ_MGR 2>/dev/null | cut -d ' ' -f 2 2>/dev/null`
fi

# Manage RabbitMQ/vCloud user password
if [ "x${VCD_PASSWD_CURRENT}" != "x" ]; then
    [ "x${VCD_PASSWD}" = "x${VCD_PASSWD_CURRENT}" -o "x${VCD_PASSWD}" = "x" ] && VCD_PASSWD_CHANGED=0
    if [ "x${VCD_PASSWD}" = "x" ]; then
        VCD_PASSWD=$VCD_PASSWD_CURRENT
    fi
fi
if [ "x${VCD_PASSWD}" = "x" ]; then
    VCD_PASSWD=`gen_random_passwd`
fi

# Manage RabbitMQ manager password
if [ "x${RBT_PASSWD_CURRENT}" != "x" ]; then
    [ "x${RBT_PASSWD}" = "x${RBT_PASSWD_CURRENT}" -o "x${RBT_PASSWD}" = "x" ] && RBT_PASSWD_CHANGED=0
    if [ "x${RBT_PASSWD}" = "x" ]; then
        RBT_PASSWD=$RBT_PASSWD_CURRENT
    fi
fi
if [ "x${RBT_PASSWD}" = "x" ]; then
    RBT_PASSWD=`gen_random_passwd`
fi

# Set parameters from config if weren't passed
if [ "x${RBT_HOST}" = "x" -a "x${RBT_HOST_CURRENT}" != "x" ]; then
    RBT_HOST=$RBT_HOST_CURRENT
fi
if [ "x${VCD_LOGIN}" = "x" -a "x${VCD_LOGIN_CURRENT}" != "x" ]; then
    VCD_LOGIN=$VCD_LOGIN_CURRENT
fi
if [ "x${RBT_LOGIN}" = "x" -a "x${RBT_LOGIN_CURRENT}" != "x" ]; then
    RBT_LOGIN=$RBT_LOGIN_CURRENT
fi
if [ "x${RBT_PASSWD}" = "x" -a "x${RBT_PASSWD_CURRENT}" != "x" ]; then
    RBT_PASSWD=$RBT_PASSWD_CURRENT
    RBT_PASSWD_CHANGED=0
fi
if [ "x${VCD_VHOST}" = "x" -a "x${VCD_VHOST_CURRENT}" != "x" ]; then
    VCD_VHOST=$VCD_VHOST_CURRENT
fi

# Set defaults if parameters aren't set on the config and weren't passed
if [ "x${VCD_LOGIN}" = "x" ]; then
    VCD_LOGIN=$VCD_LOGIN_DEFAULT
fi
if [ "x${RBT_LOGIN}" = "x" ]; then
    RBT_LOGIN=$RBT_LOGIN_DEFAULT
fi
if [ "x${RBT_HOST}" = "x" ]; then
    RBT_HOST=$RBT_HOST_DEFAULT
fi
if [ "x${VCD_VHOST}" = "x" ]; then
    VCD_VHOST=$VCD_VHOST_DEFAULT
fi

# Write new configuration of OnApp/vCloud connection to RabbitMQ into on_app.yml
[ "x${VCD_LOGIN}" != "x" -a "x${VCD_LOGIN}" != "x${VCD_LOGIN_CURRENT}" ] && manage_yml "$ONAPP_CP_CONF" "set" "rabbitmq_login" "${VCD_LOGIN}"
[ "x${VCD_PASSWD}" != "x" -a $VCD_PASSWD_CHANGED -eq 1 ] && manage_yml "$ONAPP_CP_CONF" "set" "rabbitmq_password" "${VCD_PASSWD}"
[ "x${VCD_VHOST}" != "x" -a "x${VCD_VHOST}" != "x${VCD_VHOST_CURRENT}" ] && manage_yml "$ONAPP_CP_CONF" "set" "rabbitmq_vhost" "${VCD_VHOST}"
[ "x${RBT_HOST}" != "x" -a "x${RBT_HOST}" != "x${RBT_HOST_CURRENT}" ] && manage_yml "$ONAPP_CP_CONF" "set" "rabbitmq_host" "${RBT_HOST}"

LOCALRABBITMQ=0
# If 127.0.0.1 or localhost - local RabbitMQ
if [ "x${RBT_HOST}" = "x127.0.0.1" ] || [ "x${RBT_HOST}" = "xlocalhost" ]; then
    LOCALRABBITMQ=1
fi

# if SNMP_TRAP_IPS (the IP address(es) of Control Panel ) - local RabbitMQ
if echo ${SNMP_TRAP_IPS} | grep $RBT_HOST >/dev/null 2>&1; then
    LOCALRABBITMQ=1
fi

[ $LOCALRABBITMQ -ne 1 ] && log "Seems like remote RabbitMQ is used."

if [ $LOCALRABBITMQ -eq 1 ]; then

    log "Configure rabbitmq-server to start at boot time"
    service_action "rabbitmq-server" "enable"

    if service_action "rabbitmq-server" "status" 2>/dev/null | egrep -e 'nodedown|inactive' >/dev/null 2>&1; then
        if [ $HA_ENABLED -ne 1 ]; then
            log "Starting rabbitmq-server ..."
            service_action "rabbitmq-server" "start" >/dev/null 2>&1
            if [ $? -ne 1 ]; then
                failed "1"
            fi
        else
            log "It seems HA is enabled. Will not start RabbitMQ Server services."
        fi
    fi

    if ! service_action "rabbitmq-server" "status" 2>/dev/null | egrep -e 'nodedown|inactive' >/dev/null 2>&1; then
        rabbitmq_check_user "guest"
        if [ $? -eq 0 ]; then
            log "Removing default 'guest' user"
            run_and_log "rabbitmqctl -q delete_user guest"
        fi

        rabbitmq_configure_user "${VCD_LOGIN}" "${VCD_PASSWD}" "${VCD_VHOST}" ".*" ".*" ".*" "$VCD_PASSWD_CHANGED"
        rabbitmq_configure_user "${RBT_LOGIN}" "${RBT_PASSWD}" "${VCD_VHOST}" ".*" ".*" ".*" "$RBT_PASSWD_CHANGED" "administrator"
        echo "${RBT_LOGIN} ${RBT_PASSWD}" > $RABBITMQ_MGR

        if ! rabbitmq-plugins list -m -E rabbitmq_management 2>/dev/null | grep rabbitmq_management >/dev/null 2>&1; then
            log "Enable web based management"
            run_and_log "rabbitmq-plugins enable rabbitmq_management"
            log "Restarting the rabbitmq-server ..."
            service "rabbitmq-server" "stop"
            service "rabbitmq-server" "start"
        fi
        # Determine IP address on the first ethernet interface
        IP=
        IP=`ifconfig eth0 2>/dev/null | egrep -e '[[:space:]]+inet[[:space:]]+' 2>/dev/null | awk '{print $2}' 2>/dev/null  | sed 's/addr://'`
        log "The web based management already enabled."
        log "Try to navigate http://${IP}:15672 and use login '$RBT_LOGIN' and password specified in the $RABBITMQ_MGR"
    fi
fi

log "`date '+%F %H:%M'` Finished configuring RabbiMQ for Control Panel and vCloud" "SUCCESS"
logger "$0 - Finished configuring RabbiMQ for Control Panel and vCloud"
exit 0
