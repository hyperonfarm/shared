#!/bin/bash

#CAUTION: USE ONLY WHEN YOU SURE WHAT YOU'RE DOING!
#DO NOT USE FOR HARMFUL ACTIONS

#VERSION 2 OVERHAULED

#DESCRIPTION:
#This script is a wrokaround job designed to make easier cleanup of the stuck rspamd processes and left control paths
#Just an automated implementation of the KB articles:
#REFER https://help.onapp.com/hc/en-us/community/posts/211117848-onappstore-activate-failed-for-vdisk-Failed-to-detect-control-path-for-vdisk-Integrated-storage-
#and https://help.onapp.com/hc/en-us/community/posts/211117208--IS-RepairDisk-action-failing


excecute () {

sssh () {
		ssh_exec="$(which ssh)"
		key_file="/onapp/interface/config/keys/private"
		test -f $key_file && $ssh_exec -qo PasswordAuthentication=no $@ || $ssh_exec -i $key_file $@
	}

msqlh=`grep host /onapp/interface/config/database.yml | cut -d ":" -f2 | uniq | sed "s/[',  ]//g"`
msqlp=`grep password /onapp/interface/config/database.yml | cut -d ":" -f2 | uniq | sed "s/[',  ]//g"`

viems="$@";

#viem="$(echo $viems |sed -e "s/[^ ][^ ]*/'&'/g;s/[ ]/,/g")"

viem="$(echo $viems|sed -e 's/ .*//')"

for vm_boot in $viems; do

        booted="$(mysql -h"$msqlh" -u root -p"$msqlp" onapp -e "select booted from virtual_machines where identifier='$vm_boot'\G" |grep -v '*'|sed -e 's/.*: //')"

        if [ "$booted" -eq "1" ]; then

            printf "\nALARM!!! THE VM $viem IS ONLINE!!!\n\n"

        elif [ "$booted" -eq "0" ]; then

        	mysql -h"$msqlh" -uroot -p"$msqlp" onapp -e "select identifier from disks where virtual_machine_id in (select id from virtual_machines where identifier='$vm_boot');" |grep -v identifier |xargs echo >> /tmp/temp.vdisks.query

        fi
done

sql_disks="$(cat /tmp/temp.vdisks.query 2>/dev/null|xargs echo)"

hypervisor_addr="$(mysql -h"$msqlh" -uroot -p"$msqlp" onapp -e "select ip_address from hypervisors where id in (select hypervisor_id from virtual_machines where identifier='$viem');" | grep -v "*" | cut -d":" -f2 | sed "s/[ ]//g" |grep -v ip_address)"

if [[ -z "$sql_disks" ]]; then

		printf "SORRY, NO DISKS WERE SPECIFIED!\n\n"
		exit 1
else

cat >> /tmp/temp.vdisks.clean.execute << EOF
 disks="$sql_disks"; for i in \$disks; do vdisk="\$i"; backends=\$(onappstore nodes | grep -B1 BACKEND | grep IP | sed 's/.*://'|sort -u); for i in \$backends; do echo =========\$i; ssh -o StrictHostKeyChecking=no \$i "ps w|grep \$vdisk | grep -v grep|sed 's/^ *//'|cut -d' ' -f1|xargs kill -9 2>/dev/null;find /var/run -path '*\$vdisk*' -exec rm -rf {} \;"; done; done
EOF
 
 cat /tmp/temp.vdisks.clean.execute | sssh $hypervisor_addr "cat > /tmp/temp.vdisks.clean.execute"
 sssh $hypervisor_addr "bash /tmp/temp.vdisks.clean.execute; rm -f /tmp/temp.vdisks.clean.execute"

fi

 rm -f /tmp/temp.vdisks.query
 rm -f /tmp/temp.vdisks.clean.execute

}

[[ -z "$@" ]] && printf "\nERROR!! Specify vm identifiers\n\nUsage: bash $0 vm_identifier1 vm_identifier2 ... vm_identifierN\n\n"
[[ -z "$@" ]] || excecute "$@"
