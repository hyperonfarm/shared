#!/bin/bash
#debugging variables
#set -ex
#IFS=$'\n\t'

#UPDATE VARIABLE
up_to_date="0"
#---------------

expected_args=8
opt_expected_args=11

export DEBIAN_FRONTEND=noninteractive

genpass () {
		date +%s%N | sha256sum | base64 | head -c 32 ; echo;
	   }

if [ $# -ne $expected_args ] && [ $# -ne $opt_expected_args ]; then 

echo "usage:"
printf "bash $0 system_user system_user_pass db_name db_user_name db_user_pass repo_name server_domain backup_server set_instance_per_app(optional) ruby_version(optional) rails_version(optional)\n"
echo "description:"
echo "-------------------------------------------------------------------------------------------------"
echo "system_user - operating system user"
echo "system_user_pass - operating system user password"
echo "db_name - new mysql database name"
echo "db_user_name - user for previously submitted database name"
echo "db_user_pass - password for db_user_name"
echo "repo_name - destination of the working web directory(e.g. /var/www/\$repo_name)"
echo "server_domain - domain for nginx config"
echo "backup_server - set backup server. in such format: user@host"
echo "set_instance_per_app - optional setting to configure max number of instances per passanger app. set by default to 0(unlimited)"
echo "ruby_version - optional setting to configure ruby version for deployment. set by default to 2.1.5"
echo "rails_version - optional setting to configure rails version for deployment. set by default to 4.2.1"
echo "IMPORTANT NOTE: IF YOU SET AT LEAST ONE OPTIONAL PARAMETER YOU SHOULD SET THE REST. OTHERWISE THE SCRIPT WON'T WORK."
echo "-------------------------------------------------------------------------------------------------"
echo "USAGE EXAMPLE:"
echo "bash environment.sh deploy deployuserpass deploydb deploydbuser deploydbuserpass deploy_test deploy.test deploy@backup.server.com 1 2.1.5 4.2.1"
echo "USAGE EXAMPLE WITHOUT FINGERPRINT:"
printf "wget -qO- https://gitlab.com/hyperonfarm/shared/raw/master/environment.sh | bash -s deploy deployuserpass deploydb deploydbuser deploydbuserpass deploy_test deploy.test deploy@backup.server.com 1 2.1.5 4.2.1\n"
echo "-------------------------------------------------------------------------------------------------"
else

user=${1:-""}
user_pass=${2:-""}
db_name=${3:-""}
db_user_name=${4:-""}
db_user_pass=${5:-""}
HOME="/var/www"
nginx_site="/etc/nginx"
repo_name=${6:-""}
repo_www=${6:-""}
server_domain=${7:-""}
backup_server=${8:-""}
set_instance_per_app=${9:-0}
ruby_ver=${10:-2.1.5}
rails_ver="-v ${11:-4.2.1}"
nuke_nginx=$(echo "$set_instance_per_app")

if [ $up_to_date -eq 0 ]; then

#CHECK QUOTA CONFIG
if [ $(dpkg -l | grep quota | wc -l) -ne 0 ]; then
 quotaoff -av
fi

#ADD NECESSARY SYSTEM VARIABLES
locale-gen en_US.UTF-8
cat >> /etc/environment << EOF
LC_ALL="en_US.UTF-8"
LANGUAGE="en_US.UTF-8"
HISTTIMEFORMAT="%F %T "
EOF

#CONFIGURE LIMITS
printf "nginx       soft    nofile   10000\nnginx       hard    nofile  30000\n" >> /etc/security/limits.conf

echo "net.core.somaxconn=65535" >> /etc/sysctl.conf
echo "net.ipv4.tcp_max_tw_buckets=1440000" >> /etc/sysctl.conf
echo "fs.file-max=70000" >> /etc/sysctl.conf

#IF MYSQL INSTALLED
if [ $(dpkg -l | grep mysql-server | wc -l) -ne 0 ]; then
 service mysql stop && mysqld_safe --skip-grant-tables &
 mysql --user=root -Bse "use mysql;update user set Password=PASSWORD('$user_pass') where user='root';flush privileges;"
 service mysql restart
fi

#CREATE AND SET THE SWAP
test /swap || dd if=/dev/zero of=/swap bs=1M count=1024 && mkswap /swap && swapon /swap || rm /swap
if [ -f /swap ]; then echo "/swap none swap sw 0 0" >> /etc/fstab; fi

#UPDATE SYSTEM
apt-get update && apt-get -y dist-upgrade
find . -name "$0" -exec sed -i '/find/!s/up_to_date="0"/up_to_date="1"/g' {} \;
mkdir -p /var/www/

#GENERATE PASSWORDS IF NOT SET

if [[ "$user_pass" == "deployuserpass" ]]; then user_pass="$(genpass)"; fi
if [[ "$db_user_pass" == "deploydbuserpass" ]]; then db_user_pass="$(genpass)"; fi

#ADD WORKER USER
 if [ $(grep deploy /etc/passwd | wc -l) -ne 1 ]; then
	adduser $user --disabled-password --gecos "" 2>/dev/null
	echo "$user:$user_pass" | chpasswd
	usermod -d /var/www deploy
	chown $user. /var/www/
	chmod 755 /var/www/
	#chmod g+w /var/www/
 fi

#ADD PASSENGER REPO
for i in trusty precise lucid wheezy squeeze xenial
do
 if [ $(grep $i /etc/*-release | wc -l) -gt 0 ]; then
  echo "deb https://oss-binaries.phusionpassenger.com/apt/passenger $i main" > /etc/apt/sources.list.d/passenger.list
 fi
done

#INSTALL PREREQUISITES
apt-get install -y software-properties-common python-software-properties python g++ make apt-transport-https ca-certificates
grep xenial /etc/*-release || add-apt-repository ppa:chris-lea/node.js -y
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 561F9B9CAC40B2F7
apt-get update
apt-get install -y curl \
subversion \
nodejs \
libcurl4-openssl-dev \
libmysqlclient-dev libssl-dev libapr1-dev build-essential zlib1g-dev mysql-server mysql-common mysql-client libpq-dev git nginx-extras passenger htop \
imagemagick libmagickwand-dev apache2 libapache2-svn
add-apt-repository ppa:certbot/certbot -y
apt-get update
apt-get install -y python-certbot-apache
apt-get install -y python-certbot-nginx


mysqladmin -u root password $user_pass


#INSTALL RVM RUBY AND RAILS
bash -c "curl -sSL https://rvm.io/mpapis.asc | gpg --import -; curl -L https://get.rvm.io | bash -s stable; source /usr/local/rvm/scripts/rvm; rvm requirements; rvm install ruby-$ruby_ver; rvm get stable --auto; rvm use $ruby_ver; rvm rubygems current; gem install rails $rails_ver --no-ri --no-rdoc; gem install bundler; gem install capistrano;"

if [ $(grep PATH /etc/environment | wc -l) -eq 0 ]; then 
	export PATH=$PATH:/usr/local/rvm/bin/
	echo "PATH=$PATH" >> /etc/environment
	echo '[[ -s "/usr/local/rvm/scripts/rvm" ]] && source "/usr/local/rvm/scripts/rvm"' >> $HOME/.profile 
	source /usr/local/rvm/scripts/rvm
fi

#CONFIGURE WORKER USER GROUPS AND PERMISSIONS
usermod -G $user,www-data,sudo,rvm $user 
usermod -G $user,www-data,sudo,rvm www-data
chown -R $user /usr/local/rvm

#CONFIGURE PASSENGER INSIDE THE NGINX SERVER
sed -i 's/worker_processes.*/worker_processes 1;/g' /etc/nginx/nginx.conf
sed -i 's/# passenger_root.*/passenger_root \/usr\/lib\/ruby\/vendor_ruby\/phusion_passenger\/locations.ini;/g' /etc/nginx/nginx.conf
RUBY_BINARY=$(passenger-config --ruby-command | grep Nginx | cut -d ":" -f2 | sed -e 's/\([[\/.*]\|\]\)/\\&/g')
sed -i "s/# passenger_ruby \/usr\/bin\/ruby;/$RUBY_BINARY;/g" /etc/nginx/nginx.conf
sed -i "s/# passenger_ruby \/usr\/bin\/passenger_free_ruby;/$RUBY_BINARY;/g" /etc/nginx/nginx.conf
sed -i "s/.*passenger.conf.*/include \/etc\/nginx\/passenger.conf;/g" /etc/nginx/nginx.conf #new versions, compatibility

if [ $set_instance_per_app -ne 0 ]; then
	sed -i "/passenger.conf/a passenger_max_instances_per_app $set_instance_per_app;" /etc/nginx/nginx.conf #new versions, compatibility
	sed -i "/passenger_ruby/a passenger_max_instances_per_app $set_instance_per_app;" /etc/nginx/nginx.conf
fi

#CREATE WEB CONFIG

cat > $nginx_site/sites-available/$repo_www <<EOF
server {
listen 80;
server_name $server_domain default;
passenger_enabled on;
root /var/www/$repo_name/current/public;
#passenger_env_var SECRET_KEY_BASE
}
server {
listen 80;
server_name     www.$server_domain;
return 301 \$scheme://$server_domain\$request_uri;
}
EOF
#--------------------------

#RECONFIGURE NGINX
echo "$user ALL = (root) NOPASSWD: /etc/init.d/nginx stop, /etc/init.d/nginx start" | tee >> /etc/sudoers /etc/sudoers.d/nginx
ln -s $nginx_site/sites-available/$repo_www $nginx_site/sites-enabled/$repo_www
rm /etc/nginx/sites-enabled/default

#NUKE THE NGINXCONF AND MAKE NEW CONF

if [[ "$nuke_nginx" == "0" ]]; then

cp /etc/nginx/nginx.conf{,.bak}

cat > /etc/nginx/nginx.conf <<EOF

user www-data;
worker_processes auto; 
worker_rlimit_nofile 30000;
pid /run/nginx.pid;
events {
	worker_connections 2048;
	multi_accept on;
	use epoll;
}
http {
	sendfile on;
	tcp_nopush on;
	tcp_nodelay on;
	keepalive_timeout 65;
	types_hash_max_size 2048;
	open_file_cache max=200000 inactive=20s;
	open_file_cache_valid 30s;
	open_file_cache_min_uses 2;
	open_file_cache_errors on;
	include /etc/nginx/mime.types;
	default_type application/octet-stream;
	ssl_prefer_server_ciphers on;
	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;
	gzip on;
	gzip_min_length 10240;
	gzip_proxied expired no-cache no-store private auth;
	gzip_types text/plain text/css text/xml text/javascript application/x-javascript application/xml;
	gzip_disable "MSIE [1-6]\\.";
	gzip_disable "msie6";
	
	include /etc/nginx/passenger.conf;
	passenger_max_instances_per_app 2;
	passenger_max_request_queue_size 4000;
	
	include /etc/nginx/conf.d/*.conf;
	include /etc/nginx/sites-enabled/*;
}

EOF

fi

#DISABLE APACHE
#sed -i 's/Listen 80/# Listen 80/g' /etc/apache2/ports.conf 2>/dev/null
#/etc/init.d/apache2 stop
#update-rc.d apache2 disable

#RECONFIGURE TZDATA
echo "Europe/Uzhgorod" > /etc/timezone
dpkg-reconfigure -f noninteractive tzdata

#RECONFIGURE MYSQL
sed -i "/\[client\]/a default-character-set=utf8" /etc/mysql/my.cnf
sed -i "/\[mysql\]/a default-character-set=utf8" /etc/mysql/my.cnf
sed -i "/\[mysqld\]/a collation-server=utf8_unicode_ci" /etc/mysql/my.cnf
sed -i "/collation-server=utf8_unicode_ci/a init-connect='SET NAMES utf8'" /etc/mysql/my.cnf
sed -i "/init-connect=/a character-set-server=utf8" /etc/mysql/my.cnf
echo "$user ALL = (root) NOPASSWD: /etc/init.d/mysql stop, /etc/init.d/mysql start" | tee >> /etc/sudoers /etc/sudoers.d/mysql
/etc/init.d/mysql restart

fi

#CREATE DATABASE
#mysql -uroot -p$user_pass -Bse "create database $db_name;create user $db_user@localhost identified by '$db_user_pass';grant all privileges on $db_name.* to '$db_user'@'localhost' with grant option;"
mysql -uroot -p$user_pass -Bse "create database if not exists $db_name;grant all on $db_name.* to '$db_user_name'@'localhost' identified by '$db_user_pass';flush privileges;"

#SET LOG CLEANUP
echo "0 * * * * root echo '' > /var/log/nginx/access.log" >> /etc/cron.d/cleanlogs
echo "0 * * * * root echo '' > /var/www/$repo_www/shared/log/production.log" >> /etc/cron.d/cleanlogs

#SET DATABASE DUMP
#echo "0 */4 * * * root mysqldump -uroot -p$user_pass --events --ignore-table=mysql.event --all-databases > /tmp/$server_domain.sql; cat /tmp/$server_domain.sql | ssh $backup_server 'gzip > /mnt/mifnfs/databases/$server_domain.alldb.\$(date +"\\%F-\\%H_\\%M_\\%S").sql.gz'; rm /tmp/*.sql;" >> /etc/cron.d/mysqldump && chmod 644 /etc/cron.d/mysqldump

#SET UPLOAD DUMP
#echo "0 */5 * * * root rsync -rtvuz --delete /var/www/$repo_name/current/public/uploads/ $backup_server:/mnt/mifnfs/files/$repo_name/ >> /var/log/rsync.log" >> /etc/cron.d/uploadsync && chmod 544 /etc/cron.d/uploadsync

#GENERATE CERTIFICATES FOR USER
ssh-keygen -t rsa -N "" -f /root/.ssh/id_rsa >> /dev/null
#echo -e "copy the following key to the dump destination server into the \033[31m~/.ssh/authorized_keys\033[0m file"
#cat /root/.ssh/id_rsa.pub

#DEPLOY THE SVN DAV SERVER

rm -f /etc/apache2/sites-enabled/*
cat > /etc/apache2/sites-available/subversion.conf << EOF
NameVirtualHost *:8080

<VirtualHost *:8080>   
  <Location /svn>
      ErrorDocument 404 default
      DAV svn
		SVNPath /var/www/svn
  		AuthType Basic
  		AuthName "svn_repo"
  		AuthUserFile /etc/subversion/passwd
  		Require valid-user

  </Location>
</VirtualHost>
EOF

cat > /etc/apache2/ports.conf << EOF

Listen 127.0.0.1:8080

EOF

mkdir -p /var/www/svn

a2ensite subversion.conf && service apache2 stop

#PRINT THE NECCESSARY VALUES

white_ip="$(ip a|grep inet|egrep -v 'inet6|127' |sed '2,3d;s/ brd.*.//;s/.*.inet //;s/\/24//')"

cat > /root/env.txt << EOF
ip - $white_ip 
user - $user
pass - $user_pass
mysql dbname - $db_name
mysql dbname user - $db_user_name
mysql dbname user pass - $db_user_pass
repo name - $repo_www
domain name - $server_domain
EOF

echo "cat /root/env.txt" >> /root/.bashrc

#COPY NECESSARY KEYS
#echo "* * * * * root su - deploy -c 'mkdir -p $(grep deploy /etc/passwd | cut -d ':' -f 6)/.ssh' && scp -B -q $backup_server:~/.ssh/authorized_keys /var/www/.ssh/ && chmod -R 544 /var/www/.ssh/ && rm /etc/cron.d/copykeys && rm /etc/cron.d/scanhost" > /etc/cron.d/copykeys && chmod 544 /etc/cron.d/copykeys

#SCAN AND GENERATE THE BACKUPSERVER
#echo "* * * * * root if [ -z \`ssh-keygen -F $(echo $backup_server | sed 's/.*@//')\` ]; then ssh-keyscan -H $(echo $backup_server | sed 's/.*@//') >> ~/.ssh/known_hosts; fi" > /etc/cron.d/scanhost && sed -i 's/[ ].*@/ /' /etc/cron.d/scanhost && chmod 544 /etc/cron.d/scanhost

#INSTALL ZABBIX AGENT

#apt-get install zabbix-agent && sed -i 's/Server=.*/Server=zabbix.sp-service.com.ua/' /etc/zabbix/zabbix_agentd.conf && service zabbix-agent restart

shutdown -r +1 &

fi

exit 0
